// swift-tools-version: 5.7.1
// The swift-tools-version declares the minimum version of Swift required to build this package.
//
//  Created by Gregor Longariva on 14.12.2023.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import PackageDescription

let package = Package(
    name: "dogeADAuth",
    platforms: [
        .macOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "dogeADAuth",
            // the following targets are needed. It seems, that SPM needs to separate
            // swift and ObjectiveC source code files separately
            targets: ["dogeADAuthObjC", "dogeADAuth"]),
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-docc-plugin", from: "1.0.0"),
    ],
    targets: [
        // the ObjectiveC source code
        .target(
                name: "dogeADAuthObjC",
                path: "Sources/dogeADAuthObjC",
                // it seems that the header files must be stored in a specific directory
                // the header path has to be relative to the ObjectiveC directory
                publicHeadersPath: "include"),
        // the Swift source code
        .target(
                name: "dogeADAuth",
                dependencies: ["dogeADAuthObjC"],
                path: "Sources/dogeADAuth",
                linkerSettings: [
                    .linkedFramework("Security"),
                    .linkedFramework("GSS"),
                    .linkedFramework("Kerberos"),
//                    .linkedLibrary("libcommonCrypto"),
                    .linkedLibrary("resolv"),
                ]
        ),
        // tests
        .testTarget(
            name: "dogeADAuthTests",
            path: "Sources/dogeADAuthTests"),
    ]
)
