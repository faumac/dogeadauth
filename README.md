# ``dogeADAuth``

This framework allows to easily add a robust AD integration to any macOS application.

Please refer to [dogeADAuth's documentation](https://faumac.pages.rrze.fau.de/dogeadauth/documentation/dogeadauth/)