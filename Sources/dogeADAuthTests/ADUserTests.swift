//
//  ADUserTests.swift
//  dogeADAuth
//
//  Created by Joel Rennich on 9/10/17.
//  Copyright © 2018 Joel Rennich. All rights reserved.
//

import Foundation
import XCTest
import dogeADAuthObjC
@testable import dogeADAuth

class ADUserTests : XCTestCase, dogeADUserSessionDelegate {
    func dogeADAuthenticationFailed(error: dogeADSessionError, description: String) {
        NSLog("%@", "Authentication failed called.")
    }

    
    // some setup
    
    let session = dogeADSession.init(domain: "doge.test", user: "ftest@DOGE.TEST", type: .AD)
    let session2 = dogeADSession.init(domain: "doge.test", user: "ftest2@DOGE.TEST", type: .AD)
    
    // kill any existing tickets
    
    let result = cliTask("kdestroy -a")
    
    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
    }
    
    func testAuth() {
        session.userPass = "FAUmacDogeADRocks1!"
        
        expectation = self.expectation(description: "Auth Succeeded")
        session.delegate = self
        session.authenticate()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testAuthAgain() {
        // this should not need to lookup sites
        session.userPass = "FAUmacDogeADRocks1!"
        expectation = self.expectation(description: "Auth Succeeded")
        session.delegate = self
        session.authenticate()
        self.waitForExpectations(timeout: 10, handler: nil)
        session.userInfo()
    }
    
    func testAuthFail() {
        // this should fail
        session.userPass = "NotthePassword!"
        expectation = self.expectation(description: "Auth Failed")
        session.delegate = self
        session.authenticate()
        self.waitForExpectations(timeout: 10, handler: nil)
    }

    func testUserLookup() {
        session.userInfo()
        print(session.userRecord)
        print(session.userRecord?.computedExireDate)
    }
    
    func testSecondAuth() {
        session2.userPass = "DogeAD21!"
        expectation = self.expectation(description: "Auth Succeeded")
        session2.delegate = self
        session2.authenticate()
        self.waitForExpectations(timeout: 10, handler: nil)
        session2.userInfo()
    }
    
    func testTicketList() {
        print(klistUtil.klist())
    }
    
    // MARK: Delegate
    
    func dogeADAuthenticationSucceded() {
        
        if expectation?.description == "Auth Succeeded" {
            print("Auth Succeeded")
            expectation?.fulfill()
        } else {
            XCTFail()
        }

    }
    
    func dogeADAuthenticationFailed(error: Error, description: String) {
        if expectation?.description == "Auth Failed" {
            print("Auth Failed")
            expectation?.fulfill()
        } else {
            XCTFail()
        }
    }
    
    func dogeADUserInformation(user: ADUserRecord) {
        print("***User Record for: \(user.fullName)***")
        print(user)
    }
}
