# ``dogeADAuth``

This framework allows to easily add a robust AD integration to any macOS application.

## Overview

The [doge AD authentication framework](https://faumac.pages.rrze.fau.de/dogeadauth/documentation/dogeadauth/) allows you to present a username and password to the Framework and have it get tickets for the user and then lookup the user's information in AD. In addition the framework is:

- site aware
- able to change passwords
- able to use SSL for AD lookups
- can have the site forced or ignored
- is aware of network changes, and will mark sites to be re-discovered on changes
- perform recursive group lookups
- create Kerberos tickets

This framework is a fork and further development of Joel Rennich's [NoMAD-ADAuth](https://github.com/jamf/NoMAD-ADAuth) Framework. It represents his experience and the extensive development hours dedicated to NoMAD's Active Directory authentication.    
Many thanks to Joel for his work on NoMAD and NoMAD-ADAuth! Without his ideas and goot work on [NoMAD](https://github.com/jamf/NoMAD) and [NoMAD 2](https://github.com/jamf/NoMAD-2) this Framework and our depending apps could never happen!

## Topics

### gettingStarted

### <!--@START_MENU_TOKEN@-->Group<!--@END_MENU_TOKEN@-->

- <!--@START_MENU_TOKEN@-->``Symbol``<!--@END_MENU_TOKEN@-->
