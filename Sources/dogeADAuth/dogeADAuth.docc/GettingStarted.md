# Getting Started with dogeADAuth

Add the dogeADAUth framework to your project and use it to authenticate your app against an Active Directory server.

## Overview

dogeADAuth is a Swift Package Manager (SPM) package that provides a simple interface to authenticate against an Active Directory server. It uses the LDAP protocol to communicate with the server and authenticate the user. The framework is designed to be easy to use and integrate into your project, allowing you to quickly add AD authentication to your macOS application.

### Adding dogeADAuth to your project

dogeADAuth is a [Swift Package Manager](https://www.swift.org/package-manager/)(SPM) Package and can imported as usual through *Xcode File* > *Add Package Dependencies...* using the [project URL](https://gitlab.rrze.fau.de/faumac/dogeadauth) 

### Basic Usage of the Framework via Delegate

- Drag the framework into your project in the Embedded Binaries section of the target
- Import dogeADAuth into your class
- Adopt ``dogeADUserSessionDelegate``, and then add the stubs suggested to conform to the protocol
- create a ``dogeADSession`` object
```swift
let session = dogeADSession.init(domain: "doge.test", user: "ftest@DOGE.TEST", type: .AD)
```
- set a password on the session object 
```swift
session.userPass = "FAUmacDogeADRocks1!"
```
- set the session delegate to your class 
```swift
session.delegate = self
```
- try to authenticate 
```swift
session.authenticate()
```
- the delegate callbacks will then let you know if the auth succeeded or not

### Basic Usage of the Framework via Closure

- Drag the framework into your project in the Embedded Binaries section of the target
- Import `dogeADAuth`
- Make a ``dogeADSession`` object via 
```swift
init(domain: String, user: String, type: LDAPType = .AD)
```

- Set the session's `userPass` variable
- Call the session's 
```swift
getKerberosTicket(principal: String? = nil, completion: @escaping (KerberosTicketResult) -> Void)
```
function

- If the optional `principal` parameter is supplied, this function tries to fetch an existing ticket for this principal,
-  and then if unsuccessful, continues by trying to get a new ticket
- This function shares its result by running the supplied closure upon completion
-  with ``KerberosTicketResult`` containing either an ``ADUserRecord`` on success or a ``dogeADSessionError`` on failure
