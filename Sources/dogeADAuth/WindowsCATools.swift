//
//  WindowsCATools.swift
//  doge
//
//  Created by Joel Rennich on 5/15/16.
//  Copyright © 2016 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import Security
import OSLog

/// A class for managing certificate operations with a Windows CA
class WindowsCATools {
    let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "WindowsCATools")

    // Variables
    private var api: String
    var certCSR: String
    var certTemplate: String
    var myImportError: OSStatus
    var err = OSStatus()
    var pubKey: SecKey? = nil
    var privKey: SecKey? = nil
    var pubKeyBits: CFTypeRef? = nil
    var privKeyBits: CFTypeRef? = nil
    var pubKeyData: Data? = nil
    var pubKeyDataPtr: CFData? = nil
    var uuid: CFUUID? = nil
    var uuidString: String? = nil
    var now: String? = nil
    var kPrivateKeyTag: String? = nil
    var kPublicKeyTag: String? = nil
    var sema: DispatchObject? = nil

    private let kCryptoExportImportManagerPublicKeyInitialTag = "-----BEGIN RSA PUBLIC KEY-----\n"
    private let kCryptoExportImportManagerPublicKeyFinalTag = "-----END RSA PUBLIC KEY-----\n"
    private let kCryptoExportImportManagerRequestInitialTag = "-----BEGIN CERTIFICATE REQUEST-----\n"
    private let kCryptoExportImportManagerRequestFinalTag = "-----END CERTIFICATE REQUEST-----\n"
    private let kCryptoExportImportManagerPublicNumberOfCharactersInALine = 64
    
    init(serverURL: String, template: String) {
        self.api = "\(serverURL)/certsrv/"
        uuid = CFUUIDCreate(nil)
        uuidString = CFUUIDCreateString(nil, uuid) as String?
        certCSR = ""
        certTemplate = template
        myImportError = 0
        now = String(describing: NSDate())
        kPrivateKeyTag = "de.fau.rrze.doge.CSR.privatekey." + now!
        kPublicKeyTag = "de.fau.rrze.doge.CSR.publickey." + now!
        sema = DispatchSemaphore(value: 0)
    }

    /// Asynchronous certificate enrollment
    @MainActor
    func certEnrollment() async throws -> OSStatus {
        // Generate the keypair
        genKeys()
        let myPubKeyData = getPublicKeyasData()
        let myCSRGen = CertificateSigningRequest(commonName: "FAUmac doge",
                                                 organizationName: "FAU",
                                                 organizationUnitName: "RRZE",
                                                 countryName: "DE",
                                                 cryptoAlgorithm: CryptoAlgorithm.sha1)
        let myCSR = myCSRGen.build(myPubKeyData, privateKey: privKey!)
        certCSR = PEMKeyFromDERKey(myCSR!, PEMType: "CSR")
        var myReqID = 0
        
        do {
            let (data, response) = try await submitCertAsync(certTemplate: certTemplate)
            if let response = response as? HTTPURLResponse, response.statusCode >= 200 && response.statusCode < 500 {
                logger.debug("Submitted certificate successfully.")
            }
            if let data = data {
                myReqID = try findReqID(data: data)
                let (certData, certResponse) = try await getCertAsync(certID: myReqID)
                if let certResponse = certResponse as? HTTPURLResponse, certResponse.statusCode >= 200 && certResponse.statusCode < 500 {
                    logger.debug("Retrieved certificate successfully.")
                }
                if let certData = certData {
                    let myCertRef = SecCertificateCreateWithData(nil, certData as CFData)
                    guard let certRef = myCertRef else {
                        logger.warning("Error getting certificate.")
                        return myImportError
                    }
                    let dictionary: [NSString: AnyObject] = [
                        kSecClass: kSecClassCertificate,
                        kSecReturnRef: kCFBooleanTrue,
                        kSecValueRef: certRef
                    ]
                    var mySecRef: AnyObject? = nil
                    self.myImportError = SecItemAdd(dictionary as CFDictionary, &mySecRef)
                    var myIdentityRef: SecIdentity? = nil
                    SecIdentityCreateWithCertificate(nil, certRef, &myIdentityRef)
                    // Manage Wi-Fi networks if needed
                } else {
                    logger.warning("No certificate data received.")
                }
            } else {
                logger.warning("No data received.")
            }
        } catch {
            logger.error("Error during certificate enrollment: \(error.localizedDescription)")
            throw error
        }
        return myImportError
    }

    /// Asynchronous wrapper for submitCert
    func submitCertAsync(certTemplate: String) async throws -> (Data?, URLResponse?) {
        return try await withCheckedThrowingContinuation { continuation in
            submitCert(certTemplate: certTemplate) { data, response, error in
                if let error = error {
                    continuation.resume(throwing: error)
                } else {
                    continuation.resume(returning: (data, response))
                }
            }
        }
    }

    /// Asynchronous wrapper for getCert
    func getCertAsync(certID: Int) async throws -> (Data?, URLResponse?) {
        return try await withCheckedThrowingContinuation { continuation in
            getCert(certID: certID) { data, response, error in
                if let error = error {
                    continuation.resume(throwing: error)
                } else {
                    continuation.resume(returning: (data, response))
                }
            }
        }
    }

    /// Submits the certificate request
    func submitCert(certTemplate: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let request = NSMutableURLRequest(url: (NSURL(string: "\(api)certfnsh.asp"))! as URL)
        request.httpMethod = "POST"
        let unreserved = "*-._/"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        let encodedCertRequestFinal = certCSR.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
        let body = "CertRequest=" + encodedCertRequestFinal! + "&SaveCert=yes&Mode=newreq&CertAttrib=CertificateTemplate:" + certTemplate
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = body.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest, completionHandler: completionHandler).resume()
    }

    /// Retrieves the certificate
    func getCert(certID: Int, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let request = NSMutableURLRequest(url: (NSURL(string: "\(api)certnew.cer?ReqID=" + String(certID) + "&Enc=bin"))! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest, completionHandler: completionHandler).resume()
    }

    /// Utility to generate key pairs
    func genKeys() {
        let privKeyGenDict: [String: AnyObject] = [
            kSecAttrApplicationTag as String: kPrivateKeyTag!.data(using: String.Encoding.utf8)! as AnyObject,
        ]
        let pubKeyGenDict: [String: AnyObject] = [
            kSecAttrApplicationTag as String: kPublicKeyTag!.data(using: String.Encoding.utf8)! as AnyObject,
        ]
        let keyGenDict: [String: AnyObject] = [
            kSecAttrIsExtractable as String: false as AnyObject,
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA as AnyObject,
            kSecAttrKeySizeInBits as String: "2048" as CFString,
            kSecAttrIsPermanent as String: true as AnyObject,
            kSecAttrLabel as String: "doge" as AnyObject,
            kSecPrivateKeyAttrs as String: privKeyGenDict as CFDictionary,
            kSecPublicKeyAttrs as String: pubKeyGenDict as CFDictionary,
        ]
        err = SecKeyGeneratePair(keyGenDict as CFDictionary, &pubKey, &privKey)
        if let errorMessage = SecCopyErrorMessageString(err, nil) {
            logger.error("Key generation error: \(errorMessage)")
        }
    }

    /// Retrieves the public key as Data
    func getPublicKeyasData() -> Data {
        let pubKeyDict: [String: AnyObject] = [
            kSecClass as String: kSecClassKey as AnyObject,
            kSecReturnData as String: true as AnyObject,
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA as AnyObject,
            kSecAttrApplicationTag as String: kPublicKeyTag!.data(using: String.Encoding.utf8)! as AnyObject,
        ]
        err = SecItemExport(pubKey!, SecExternalFormat.formatBSAFE, .init(rawValue: 0), nil, &pubKeyDataPtr)
        err = SecItemCopyMatching(pubKeyDict as CFDictionary, &pubKeyBits)
        pubKeyData = (pubKeyDataPtr! as Data)
        return pubKeyDataPtr! as Data
    }

    /// Converts DER key to PEM format
    func PEMKeyFromDERKey(_ data: Data, PEMType: String) -> String {
        var resultString: String
        let base64EncodedString = data.base64EncodedString(options: [])
        var currentLine = ""

        if PEMType == "RSA" {
            resultString = kCryptoExportImportManagerPublicKeyInitialTag
        } else {
            resultString = kCryptoExportImportManagerRequestInitialTag
        }

        var charCount = 0
        for character in base64EncodedString {
            charCount += 1
            currentLine.append(character)
            if charCount == kCryptoExportImportManagerPublicNumberOfCharactersInALine {
                resultString += currentLine + "\n"
                charCount = 0
                currentLine = ""
            }
        }
        if currentLine.count > 0 { resultString += currentLine + "\n" }
        if PEMType == "RSA" {
            resultString += kCryptoExportImportManagerPublicKeyFinalTag
        } else {
            resultString += kCryptoExportImportManagerRequestFinalTag
        }
        return resultString
    }

    /// Finds the request ID from the response data
    func findReqID(data: Data) throws -> Int {
        guard let response = String(data: data, encoding: .utf8) else {
            throw NSError(domain: "WindowsCATools", code: -1, userInfo: [NSLocalizedDescriptionKey: "Invalid response data"])
        }
        var myresponse = "0"
        if response.contains("certnew.cer?ReqID=") {
            let responseLines = response.components(separatedBy: "\n")
            let reqIDRegEx = try NSRegularExpression(pattern: ".*ReqID=", options: .caseInsensitive)
            let reqIDRegExEnd = try NSRegularExpression(pattern: "&amp.*", options: .caseInsensitive)

            for line in responseLines {
                if line.contains("certnew.cer?ReqID=") {
                    myresponse = reqIDRegEx.stringByReplacingMatches(in: line, options: [], range: NSMakeRange(0, line.count), withTemplate: "")
                    myresponse = reqIDRegExEnd.stringByReplacingMatches(in: myresponse, options: [], range: NSMakeRange(0, myresponse.count), withTemplate: "").replacingOccurrences(of: "\r", with: "")
                    return Int(myresponse) ?? 0
                }
            }
        }
        return Int(myresponse) ?? 0
    }

    /// Placeholder for 802.1x configuration
    func build8021x() {
        // Implement the method according to requirements
    }
}
