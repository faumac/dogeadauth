//
//  Extensions.swift
//  dogeADAuth
//
//  Created by Boushy, Phillip on 10/4/16.
//  Copyright © 2016 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation

// Bitwise convenience
prefix operator ~~

prefix func ~~(value: Int) -> Bool {
    return value > 0
}

extension UserDefaults {
    /// Retrieves an integer value for the specified key from the storage.
    /// If the value is stored as a string, it attempts to convert it to an integer.
    ///
    /// - Parameter defaultName: The key for which to retrieve the integer value.
    /// - Returns: An optional integer if the key exists and can be converted to an integer, otherwise nil.
    func sint(forKey defaultName: String) -> Int? {
        let item = object(forKey: defaultName)
        
        // Attempt to cast the item directly to Int
        if let result = item as? Int {
            return result
        }
        
        // Attempt to convert the item to Int if it's a String
        if let stringResult = item as? String, let intValue = Int(stringResult) {
            return intValue
        }
        
        // Return nil if the item cannot be converted to Int
        return nil
    }
}

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
//    func trim() -> String {
//        return self.trimmingCharacters(in: .whitespaces)
//    }
    
    func containsIgnoringCase(_ find: String) -> Bool {
        return range(of: find, options: .caseInsensitive) != nil
    }
    
    /*
    // TODO: move this to UserInfo
    func variableSwap() -> String {
        var cleanString = self
        
        let domain = UserDefaults.standard.string(forKey: Preferences.aDDomain) ?? ""
        let fullName = UserDefaults.standard.string(forKey: Preferences.displayName)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let serial = getSerial().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let shortName = UserDefaults.standard.string(forKey: Preferences.userShortName) ?? ""
        let upn = UserDefaults.standard.string(forKey: Preferences.userUPN) ?? ""
        let email = UserDefaults.standard.string(forKey: Preferences.userEmail) ?? ""
        
        cleanString = cleanString.replacingOccurrences(of: "<<domain>>", with: domain)
        cleanString = cleanString.replacingOccurrences(of: "<<fullname>>", with: fullName)
        cleanString = cleanString.replacingOccurrences(of: "<<serial>>", with: serial)
        cleanString = cleanString.replacingOccurrences(of: "<<shortname>>", with: shortName)
        cleanString = cleanString.replacingOccurrences(of: "<<upn>>", with: upn)
        cleanString = cleanString.replacingOccurrences(of: "<<email>>", with: email)
        
        return cleanString
    }
    */
}
