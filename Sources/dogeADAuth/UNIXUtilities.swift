//
//  NSTaskWrapper.swift
//
//  Created by Joel Rennich on 3/29/16.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import SystemConfiguration
import IOKit
import OSLog

/// A simple wrapper around Process to run command line tasks asynchronously
///
/// - Parameters:
///   - command: The `String` of the command to run. A full path to the binary is not required. Arguments can be in the main string.
///   - arguments: An optional `[String]` that represent the arguments given to the command. Defaults to `nil`.
///   - waitForTermination: An optional `Bool` Should the output be delayed until the task exits. Defaults to `true`.
/// - Returns: The combined result of standard output and standard error from the command.
public func cliTask(_ command: String, arguments: [String]? = nil, waitForTermination: Bool = true) async -> String {
    var commandLaunchPath: String
    var commandPieces: [String]
    
    if let args = arguments {
        commandLaunchPath = command
        commandPieces = args
    } else {
        commandPieces = command.components(separatedBy: " ")
        commandLaunchPath = commandPieces.remove(at: 0)
        
        if command.contains("\\") {
            for i in commandPieces.indices {
                if commandPieces[i].last == "\\" {
                    commandPieces[i] = commandPieces[i].replacingOccurrences(of: "\\", with: "") + (commandPieces.isEmpty ? "" : commandPieces.remove(at: i))
                }
            }
        }
    }

    if !commandLaunchPath.contains("/") {
        commandLaunchPath = which(commandLaunchPath)
    }

    let process = Process()
    let outputPipe = Pipe()
    let errorPipe = Pipe()
    
    process.executableURL = URL(fileURLWithPath: commandLaunchPath)
    process.arguments = commandPieces
    process.standardOutput = outputPipe
    process.standardError = errorPipe

    do {
        try process.run()
    } catch {
        return ""
    }

    if waitForTermination {
        process.waitUntilExit()
    }

    let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
    let errorData = errorPipe.fileHandleForReading.readDataToEndOfFile()

    let output = String(data: outputData, encoding: .utf8) ?? ""
    let errorOutput = String(data: errorData, encoding: .utf8) ?? ""
    
    return output + errorOutput
}

/// A simple wrapper around Process to run command line tasks synchronously
///
/// - Parameters:
///   - command: The `String` of the command to run. A full path to the binary is not required. Arguments can be in the main string.
///   - arguments: An optional `[String]` that represent the arguments given to the command. Defaults to `nil`.
///   - waitForTermination: An optional `Bool` Should the output be delayed until the task exits. Defaults to `true`.
/// - Returns: The combined result of standard output and standard error from the command.
public func cliTaskSync(_ command: String, arguments: [String]? = nil, waitForTermination: Bool = true) -> String {
    var commandLaunchPath: String
    var commandPieces: [String]

    if let args = arguments {
        commandLaunchPath = command
        commandPieces = args
    } else {
        commandPieces = command.components(separatedBy: " ")
        commandLaunchPath = commandPieces.remove(at: 0)
        
        if command.contains("\\") {
            for i in commandPieces.indices {
                if commandPieces[i].last == "\\" {
                    commandPieces[i] = commandPieces[i].replacingOccurrences(of: "\\", with: "") + (commandPieces.isEmpty ? "" : commandPieces.remove(at: i))
                }
            }
        }
    }

    if !commandLaunchPath.contains("/") {
        commandLaunchPath = which(commandLaunchPath)
    }

    let process = Process()
    process.executableURL = URL(fileURLWithPath: commandLaunchPath)
    process.arguments = commandPieces
    let outputPipe = Pipe()
    let errorPipe = Pipe()
    
    process.standardOutput = outputPipe
    process.standardError = errorPipe

    do {
        try process.run()
    } catch {
        return ""
    }

    if waitForTermination {
        process.waitUntilExit()
    }

    let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
    let errorData = errorPipe.fileHandleForReading.readDataToEndOfFile()

    let output = String(data: outputData, encoding: .utf8) ?? ""
    let errorOutput = String(data: errorData, encoding: .utf8) ?? ""
    
    return output + errorOutput
}

/// Runs a command line task asynchronously and calls a completion handler with the result
///
/// - Parameters:
///   - command: The `String` of the command to run.
///   - arguments: An optional `[String]` that represent the arguments given to the command.
///   - completion: A closure that is called with the combined result of standard output and standard error.
public func cliTask(_ command: String, arguments: [String]? = nil, completion: @escaping ((String) -> Void)) {
    var commandLaunchPath: String
    var commandPieces: [String]

    if let args = arguments {
        commandLaunchPath = command
        commandPieces = args
    } else {
        commandPieces = command.components(separatedBy: " ")
        commandLaunchPath = commandPieces.remove(at: 0)
        
        if command.contains("\\") {
            for i in commandPieces.indices {
                if commandPieces[i].last == "\\" {
                    commandPieces[i] = commandPieces[i].replacingOccurrences(of: "\\", with: "") + (commandPieces.isEmpty ? "" : commandPieces.remove(at: i))
                }
            }
        }
    }

    if !commandLaunchPath.contains("/") {
        commandLaunchPath = which(commandLaunchPath)
    }

    let process = Process()
    let outputPipe = Pipe()
    let errorPipe = Pipe()
    
    process.executableURL = URL(fileURLWithPath: commandLaunchPath)
    process.arguments = commandPieces
    process.standardOutput = outputPipe
    process.standardError = errorPipe

    process.terminationHandler = { _ in
        let data = outputPipe.fileHandleForReading.readDataToEndOfFile()
        let errorData = errorPipe.fileHandleForReading.readDataToEndOfFile()

        guard let output = String(data: data, encoding: .utf8),
              let errorOutput = String(data: errorData, encoding: .utf8) else {
            completion("")
            return
        }
        completion(output + errorOutput)
    }

    do {
        try process.run()
    } catch {
        completion("")
    }
}

/// Runs a command line task without waiting for termination
///
/// - Parameter command: The `String` of the command to run.
/// - Returns: The standard output from the command.
@available(*, deprecated)
public func cliTaskNoTerm(_ command: String) -> String {
    var commandPieces = command.components(separatedBy: " ")

    if command.contains("\\") {
        for i in commandPieces.indices {
            if commandPieces[i].last == "\\" {
                commandPieces[i] = commandPieces[i].replacingOccurrences(of: "\\", with: "") + (commandPieces.isEmpty ? "" : commandPieces.remove(at: i))
            }
        }
    }

    var commandLaunchPath = commandPieces.remove(at: 0)

    if !commandLaunchPath.contains("/") {
        commandLaunchPath = which(commandLaunchPath)
    }

    let process = Process()
    let outputPipe = Pipe()
    
    process.executableURL = URL(fileURLWithPath: commandLaunchPath)
    process.arguments = commandPieces
    process.standardOutput = outputPipe
    
    do {
        try process.run()
    } catch {
        return ""
    }

    let data = outputPipe.fileHandleForReading.readDataToEndOfFile()
    return String(data: data, encoding: .utf8) ?? ""
}

/// Retrieves the current console user
///
/// - Returns: The username of the current console user, or an empty string if not found.
public func getConsoleUser() -> String {
    var uid: uid_t = 0
    var gid: gid_t = 0

    guard let userName = SCDynamicStoreCopyConsoleUser(nil, &uid, &gid) else {
        return ""
    }
    return userName as String
}

/// Retrieves the serial number of the device
///
/// - Returns: The serial number as a `String`, or an empty string if not found.
public func getSerial() -> String {
    if #available(macOS 12.0, *) {
        let platformExpert = IOServiceGetMatchingService(kIOMainPortDefault, IOServiceMatching("IOPlatformExpertDevice"))
        
        defer { IOObjectRelease(platformExpert) }
        
        guard let serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert, kIOPlatformSerialNumberKey as CFString, kCFAllocatorDefault, 0)?
            .takeUnretainedValue() as? String else { return "" }
        
        return serialNumberAsCFString
    } else {
        let platformExpert: io_service_t = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOPlatformExpertDevice"))
        let platformSerialNumberKey = kIOPlatformSerialNumberKey
        let serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert, platformSerialNumberKey as CFString, kCFAllocatorDefault, 0)
        let serialNumber = serialNumberAsCFString?.takeUnretainedValue() as! String
        return serialNumber
    }
}

/// Retrieves the MAC address of the device
///
/// - Returns: The MAC address as a `String`, or an empty string if not found.
public func getMAC() async -> String {
    do {
        let myMACOutput = try await cliTask("/sbin/ifconfig -a")
        for line in myMACOutput.components(separatedBy: "\n") {
            if line.contains("ether") {
                return line.replacingOccurrences(of: "ether", with: "").trimmingCharacters(in: .whitespaces)
            }
        }
    } catch {
        print("Error executing ifconfig: \(error)")
    }
    return ""
}

/// Finds the full path of a command using `which`
///
/// - Parameter command: The command to find.
/// - Returns: The full path to the command, or an empty string if not found.
private func which(_ command: String) -> String {
    let task = Process()
    task.executableURL = URL(fileURLWithPath: "/usr/bin/which")
    task.arguments = [command]

    let whichPipe = Pipe()
    task.standardOutput = whichPipe

    do {
        try task.run()
    } catch {
        return ""
    }

    let data = whichPipe.fileHandleForReading.readDataToEndOfFile()
    guard let output = String(data: data, encoding: .utf8), !output.isEmpty else {
        return ""
    }

    return output.components(separatedBy: "\n").first ?? ""
}
