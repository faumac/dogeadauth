//
//  SiteManager.swift
//  dogeADAuth
//
//  Created by Joel Rennich on 9/11/17.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import SystemConfiguration
import dogeADAuthObjC
import OSLog


// Singleton für die Klasse
let siteManager = SiteManager()

// Globale Variablen
private var updatePending = false
private var updateTimer: Timer? = nil
private let logger = Logger(subsystem: Bundle.main.bundleIdentifier ?? "de.fau.rrze.dogeADAuth.SiteManager", category: "SiteManager")

/// Notification names used by SiteManager
public enum SiteManagerNotification {
    /// Notification sent when sites need to be updated
    static let updateNow = Notification.Name("de.fau.rrze.dogeADAuth.updateNow")
}

/// Manages Active Directory sites and their LDAP servers
public final class SiteManager {
    
    // MARK: - Types
    
    /// Errors that can occur in SiteManager
    public enum SiteManagerError: Error {
        case dynamicStoreCreationFailed
        case networkMonitoringFailed
    }
    
    // MARK: - Properties
    
    /// Shared instance of SiteManager
    public static let shared = SiteManager()
    
    /// Dictionary mapping domains to their LDAP servers
    private(set) var sites = [String: [dogeADLDAPServer]]()
    
    /// Logger instance for SiteManager
    private let logger = Logger(
        subsystem: Bundle.main.bundleIdentifier ?? "de.fau.rrze.dogeADAuth.SiteManager",
        category: "SiteManager"
    )
    
    /// Indicates if an update is pending
    private var updatePending = false
    
    /// Timer for delayed updates
    private var updateTimer: Timer?
    
    /// Dynamic store reference for network monitoring
    private var dynamicStore: SCDynamicStore?
    
    // MARK: - Initialization
    
    init() {
        setupNetworkMonitoring()
    }
    
    // MARK: - Public Methods
    
    /// Updates the LDAP servers for a specific domain
    /// - Parameters:
    ///   - domain: The domain to update
    ///   - hosts: Array of LDAP servers for the domain
    public func updateSites(for domain: String, with hosts: [dogeADLDAPServer]) {
        sites[domain] = hosts
        logger.debug("Updated sites for domain: \(domain), hosts count: \(hosts.count)")
    }
    
    /// Clears all stored sites
    @objc public func clearSites() {
        sites.removeAll()
        logger.info("Cleared all sites")
    }
    
    // MARK: - Private Methods
    
    /// Sets up network change monitoring
    private func setupNetworkMonitoring() {
        var dynamicContext = SCDynamicStoreContext(
            version: 0,
            info: UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque()),
            retain: nil,
            release: nil,
            copyDescription: nil
        )
        
        // Create dynamic store for network monitoring
        guard let store = SCDynamicStoreCreate(
            kCFAllocatorDefault,
            "de.fau.rrze.doge.networknotification" as CFString,
            networkChangeCallback,
            &dynamicContext
        ) else {
            logger.error("Failed to create SCDynamicStore")
            return
        }
        
        self.dynamicStore = store
        
        // Set up notification keys for IPv4 and IPv6 changes
        let notificationKeys = [
            "State:/Network/Global/IPv4",
            "State:/Network/Global/IPv6"
        ] as CFArray
        
        guard SCDynamicStoreSetNotificationKeys(store, nil, notificationKeys) else {
            logger.error("Failed to set notification keys")
            return
        }
        
        // Create and add run loop source
        guard let runLoopSource = SCDynamicStoreCreateRunLoopSource(kCFAllocatorDefault, store, 0) else {
            logger.error("Failed to create run loop source")
            return
        }
        
        CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, .defaultMode)
        
        // Set up notification observer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(clearSites),
            name: SiteManagerNotification.updateNow,
            object: nil
        )
        
        logger.debug("Network monitoring setup completed")
    }
    
    /// Callback for network changes
    private let networkChangeCallback: SCDynamicStoreCallBack = { (_, _, context) in
        guard let context = context else { return }
        
        let siteManager = Unmanaged<SiteManager>.fromOpaque(context).takeUnretainedValue()
        siteManager.handleNetworkChange()
    }
    
    /// Handles network change events
    private func handleNetworkChange() {
        logger.debug("Network change detected")
        
        // Cancel any pending update
        updateTimer?.invalidate()
        
        // Schedule new update with delay to prevent multiple rapid updates
        updateTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { [weak self] _ in
            self?.performUpdate()
        }
    }
    
    /// Performs the actual update after a network change
    private func performUpdate() {
        guard !updatePending else {
            logger.debug("Update already pending, skipping")
            return
        }
        
        updatePending = true
        
        let notification = Notification(name: SiteManagerNotification.updateNow)
        NotificationQueue.default.enqueue(notification, postingStyle: .now)
        
        updatePending = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        updateTimer?.invalidate()
    }
}

// MARK: - Thread Safety Extension
extension SiteManager {
    /// Thread-safe access to sites
    public func getSites(for domain: String) -> [dogeADLDAPServer]? {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        return sites[domain]
    }
}
