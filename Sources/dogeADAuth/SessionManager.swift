//
//  SessionManager.swift
//  dogeADAuth
//
//  Created by Joel Rennich on 11/10/17.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import dogeADAuthObjC

/// Data structure for managing user sessions
public struct DogeADSessionUserObject {
    var userPrincipal: String
    var session: dogeADSession
    var aging: Bool
    var expiration: Date?
    var daysToGo: Int?
    var userInfo: ADUserRecord?
}

/// Class for managing multiple AD sessions concurrently
public actor SessionManager: dogeADUserSessionDelegate {
    
    /// Dictionary to store sessions, indexed by username
    public var sessions = [String: DogeADSessionUserObject]()
    
    private let dateFormatter: DateFormatter
    private let workQueue = DispatchQueue(label: "de.fau.rrze.dogeADAuth.sessionmanager.background_work_queue", attributes: [])
    
    /// Initializes the `SessionManager` and retrieves the list of current users
    private init() async {
        // Initialize the date formatter
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateStyle = .medium
        self.dateFormatter.timeStyle = .short
        
        // Retrieve all current principals with tickets
        await self.getList()
    }
    
    /// Updates the information of a specific user
    /// - Parameter user: The username whose information should be updated
    public func update(user: String) async {
        guard let userSession = sessions[user]?.session else {
            // The user is not yet known
            return
        }
        
        userSession.delegate = self
        await userSession.getUserInformation()
    }
    
    /// Updates the information of all known users
    public func updateAll() async {
        guard !sessions.isEmpty else {
            // No sessions available
            return
        }
        
        for session in sessions.values {
            session.session.delegate = self
            await session.session.getUserInformation()
        }
    }
    
    /// Retrieves the list of new users
    public func getList() async {
        await klistUtil.klist()
        let principals = await klistUtil.returnPrincipals()
        
        for user in principals where sessions[user] == nil {
            guard let domain = user.components(separatedBy: "@").last?.lowercased() else {
                continue
            }
            let userSession = dogeADSession(domain: domain, user: user, type: .AD)
            
            userSession.delegate = self
            await userSession.getUserInformation()
            
            sessions[user] = DogeADSessionUserObject(
                userPrincipal: user,
                session: userSession,
                aging: false,
                expiration: nil,
                daysToGo: nil,
                userInfo: nil
            )
        }
    }
    
    /// Manually adds a user with a session
    /// - Parameters:
    ///   - user: The username
    ///   - session: The associated `dogeADSession`
    ///   - update: Indicates whether to immediately update the user information
    public func createEntry(user: String, session: dogeADSession, update: Bool = true) async {
        sessions[user] = DogeADSessionUserObject(
            userPrincipal: user,
            session: session,
            aging: false,
            expiration: nil,
            daysToGo: nil,
            userInfo: nil
        )
        
        if update {
            session.delegate = self
            await session.getUserInformation()
        }
    }
    
    // MARK: - Authentication Callbacks
    
    /// Called when authentication succeeds
    public func dogeADAuthenticationSucceded() {
        // Authentication is not handled here
    }
    
    /// Called when authentication fails
    /// - Parameters:
    ///   - error: The error that occurred
    ///   - description: A description of the error
    public func dogeADAuthenticationFailed(error: dogeADSessionError, description: String) {
        // Authentication is not handled here
    }
    
    /// Updates user information after successful authentication
    /// - Parameter user: The `ADUserRecord` of the user
    public func dogeADUserInformation(user: ADUserRecord) {
        guard var sessionUser = sessions[user.userPrincipal] else {
            return
        }
        
        if let passwordExpire = user.passwordExpire, let passwordAging = user.passwordAging, passwordAging {
            sessionUser.daysToGo = Int(passwordExpire.timeIntervalSince(Date()) / 86400)
            sessionUser.expiration = passwordExpire
            sessionUser.aging = true
        } else {
            sessionUser.aging = false
        }
        
        sessions[user.userPrincipal] = sessionUser
    }
}
