//
//  ADLDAPPing.swift
//  dogeADAuth
//
//  Created by Michael Lynn, Phillip Boushy on 10/8/16.
//  Copyright © 2016 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import OSLog

/// Represents the DS_FLAGS used in the ADLDAPPing class.
struct DS_FLAGS: OptionSet {
    let rawValue: UInt32
    init(rawValue value: UInt32) { rawValue = value }

    static let DS_PDC_FLAG = DS_FLAGS(rawValue: 1 << 0)
    static let DS_GC_FLAG = DS_FLAGS(rawValue: 1 << 2)
    static let DS_LDAP_FLAG = DS_FLAGS(rawValue: 1 << 3)
    static let DS_KDC_FLAG = DS_FLAGS(rawValue: 1 << 5)
    static let DS_TIMESERV_FLAG = DS_FLAGS(rawValue: 1 << 6)
    static let DS_CLOSEST_FLAG = DS_FLAGS(rawValue: 1 << 7)
    static let DS_WRITABLE_FLAG = DS_FLAGS(rawValue: 1 << 8)
    static let DS_GOOD_TIMESERV_FLAG = DS_FLAGS(rawValue: 1 << 9)
    static let DS_NDNC_FLAG = DS_FLAGS(rawValue: 1 << 10)
    static let DS_SELECT_SECRET_DOMAIN_6_FLAG = DS_FLAGS(rawValue: 1 << 11)
    static let DS_FULL_SECRET_DOMAIN_6_FLAG = DS_FLAGS(rawValue: 1 << 12)
    static let DS_WS_FLAG = DS_FLAGS(rawValue: 1 << 13)
    static let DS_DS_8_FLAG = DS_FLAGS(rawValue: 1 << 14)
    static let DS_DS_9_FLAG = DS_FLAGS(rawValue: 1 << 15)
    static let DS_DNS_CONTROLLER_FLAG = DS_FLAGS(rawValue: 1 << 29)
    static let DS_DNS_DOMAIN_FLAG = DS_FLAGS(rawValue: 1 << 30)
    static let DS_DNS_FOREST_FLAG = DS_FLAGS(rawValue: 1 << 31)
}

/// Represents an AD LDAP Ping response and provides methods for decoding the response data.
class ADLDAPPing {
    var type: UInt32
    var flags: DS_FLAGS
    var domainGUID: UUID
    var forest: String
    var domain: String
    var hostname: String
    var netbiosDomain: String
    var netbiosHostname: String
    var user: String
    var clientSite: String
    var serverSite: String

    private let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "ADLDAPPing")

    /// Decodes a GUID from the given buffer starting at the specified index.
    /// - Parameters:
    ///   - buffer: The data buffer containing the GUID.
    ///   - start: The starting index of the GUID in the buffer.
    /// - Returns: The decoded GUID as a UUID.
    class func decodeGUID(_ buffer: Data, start: Int) -> UUID {
        let bytes = buffer[start..<(start+16)]
        return NSUUID(uuidBytes: Array(bytes)) as UUID
    }

    /// Decodes a UInt32 value from the given buffer starting at the specified index.
    /// - Parameters:
    ///   - buffer: The data buffer containing the UInt32 value.
    ///   - start: The starting index of the UInt32 value in the buffer.
    /// - Returns: The decoded UInt32 value.
    class func decodeUInt32(_ buffer: Data, start: Int) -> UInt32 {
        let range = start..<start + 4
        return buffer.subdata(in: range).withUnsafeBytes { $0.load(as: UInt32.self) }
    }

    /// Represents the possible errors that can occur during RFC1035 decoding.
    enum DecodeError: Error {
        case illegalTag
        case cyclicPointer
    }

    /// Decodes an RFC1035 formatted string from the given buffer starting at the specified index.
    /// - Parameters:
    ///   - buffer: The data buffer containing the RFC1035 formatted string.
    ///   - start: The starting index of the string in the buffer.
    ///   - seen: A set of previously seen pointers to detect cyclic references.
    /// - Returns: A tuple containing the decoded string and the updated cursor position.
    /// - Throws: `DecodeError` if an illegal tag or cyclic pointer is encountered during decoding.
    class func decodeRFC1035(_ buffer: Data, start: UInt16, seen: Set<UInt16>? = nil) throws -> (String, UInt16) {
        let marker: UInt8 = 0xc0
        var cursor: UInt16 = start
        var result = [String]()
        var pointers = seen ?? Set<UInt16>()

        while true {
            guard let tag = buffer.subdata(in: Int(cursor)..<Int(cursor+1)).first else {
                throw DecodeError.illegalTag
            }
            cursor += 1

            if tag == 0 {
                break
            } else if let byte = buffer.subdata(in: Int(cursor)..<Int(cursor+1)).first, (tag & marker) == marker {
                cursor += 1
                let ptr: UInt16 = UInt16(tag & ~marker) << 8 | UInt16(byte)
                if pointers.contains(ptr) {
                    throw DecodeError.cyclicPointer
                }
                pointers.insert(ptr)
                let (sresult, _) = try decodeRFC1035(buffer, start: ptr, seen: pointers)
                result.append(sresult)
                break
            } else if (tag & marker) > 0 {
                throw DecodeError.illegalTag
            } else {
                let strData = buffer[Int(cursor)..<Int(cursor + UInt16(tag))]
                if let str = String(data: strData, encoding: .utf8) {
                    result.append(str)
                }
                cursor += UInt16(tag)
            }
        }

        return (result.joined(separator: "."), cursor)
    }

    /// Initializes an ADLDAPPing instance from the given base64-encoded LDAP ping response.
    /// - Parameter ldapPingBase64String: The base64-encoded LDAP ping response string.
    init?(ldapPingBase64String: String) {
        guard let netlogonData = Data(base64Encoded: ldapPingBase64String, options: []) else {
            logger.info("Netlogon base64 encoded string is invalid.")
            return nil
        }

        var cursor = UInt16(24)

        type = ADLDAPPing.decodeUInt32(netlogonData, start: 0)
        flags = DS_FLAGS(rawValue: ADLDAPPing.decodeUInt32(netlogonData, start: 4))
        domainGUID = ADLDAPPing.decodeGUID(netlogonData, start: 8)

        do {
            (forest, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (domain, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (hostname, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (netbiosDomain, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (netbiosHostname, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (user, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (serverSite, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
            (clientSite, cursor) = try ADLDAPPing.decodeRFC1035(netlogonData, start: cursor)
        } catch DecodeError.cyclicPointer {
            logger.error("Decoding RFC1035 string created a cyclic pointer loop.")
            return nil
        } catch DecodeError.illegalTag {
            logger.error("Decoding RFC1035 string encountered an illegal tag.")
            return nil
        } catch {
            logger.error("Unexpected error during decoding: \(error.localizedDescription)")
            return nil
        }

        logger.debug("Is PDC: \(self.flags.contains(.DS_PDC_FLAG)), Is GC: \(self.flags.contains(.DS_GC_FLAG)), Is LDAP: \(self.flags.contains(.DS_LDAP_FLAG)), Is Writable: \(self.flags.contains(.DS_WRITABLE_FLAG)), Is Closest: \(self.flags.contains(.DS_CLOSEST_FLAG))")
    }
}
