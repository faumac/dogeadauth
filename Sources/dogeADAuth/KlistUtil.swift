//
//  KlistUtil.swift
//  dogeADAuth
//
//  Created by Joel Rennich on 7/18/16.
//  Copyright © 2016 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import GSS
import dogeADAuthObjC
import OSLog

/// Structure representing a Kerberos ticket
public struct Ticket {
    var expired: Bool
    var expires: Date
    var defaultCache: Bool
    public var principal: String
    var krb5Cache: krb5_ccache?
    var GSSItem: GSSItemRef?
}

/// Structure representing a Kerberos ticket with minimal information
public struct KerbTicket {
    public let principal: String
    let expiration: Date
    let defaultCache: Bool
}

/// Singleton instance of `KlistUtil`
public let klistUtil = KlistUtil()

/// Class to manage Kerberos tickets
public actor KlistUtil {
    
    let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "KlistUtil")
    
    var dateFormatter = DateFormatter()
    public var tickets = [String: Ticket]()
    
    public var defaultPrincipal: String?
    public var defaultExpires: Date?
    
    public init() {
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
    }
    
    /// Returns all Kerberos tickets
    public func returnTickets() async -> [Ticket] {
        await klist()
        return Array(tickets.values)
    }
    
    /// Returns all principals
    public func returnPrincipals() async -> [String] {
        await klist()
        return tickets.keys.sorted()
    }
    
    /// Returns the default principal
    public func returnDefaultPrincipal() -> String {
        return defaultPrincipal ?? "No Ticket"
    }
    
    /// Returns the default expiration date
    public func returnDefaultExpiration() -> Date? {
        return defaultExpires
    }

    /// Retrieves and updates the list of Kerberos tickets
    @discardableResult public func klist() async -> [KerbTicket] {
        tickets.removeAll()
        defaultPrincipal = nil
        defaultExpires = nil
        
        var kerbTickets = [KerbTicket]()
        
        var context: krb5_context? = nil
        krb5_init_secure_context(&context)
        
        var oCache: krb5_ccache? = nil
        var cursor: krb5_cccol_cursor? = nil
        var ret: krb5_error_code? = nil
        var min_stat = OM_uint32()
        
        ret = krb5_cccol_cursor_new(context, &cursor)
        
        while ((krb5_cccol_cursor_next(context, cursor, &oCache) == 0) && oCache != nil) {
            let name = String(cString: krb5_cc_get_name(context, oCache))
            var krb5Principal: krb5_principal? = nil
            ret = krb5_cc_get_principal(context, oCache, &krb5Principal)
            var krb5PrincName: UnsafeMutablePointer<Int8>? = nil
            
            guard let principal = krb5Principal else {
                logger.debug("Principal is nil, unable to get principal name")
                krb5_cc_destroy(context, oCache)
                continue
            }
            
            krb5_unparse_name(context, principal, &krb5PrincName)
            guard let princName = krb5PrincName else {
                logger.debug("Principal Name is nil, unable to get tickets")
                continue
            }
            
            let princNameString = String(cString: princName)
            tickets[princNameString] = Ticket(expired: true, expires: Date.distantPast, defaultCache: false, principal: princNameString, krb5Cache: oCache, GSSItem: nil)
            
            if name == defaultPrincipal {
                defaultPrincipal = princNameString
                defaultExpires = Date.distantPast
                tickets[princNameString]?.defaultCache = true
            }
        }
        
        // Move to GSS APIs to get expiration times
        gss_iter_creds(&min_stat, 0, nil, { a, cred in
            var min_stat1 = OM_uint32()
            
            if let cred = cred {
                let name = GSSCredentialCopyName(cred)
                if let name = name {
                    let displayName = GSSNameCreateDisplayString(name)!
                    let displayNameString = String(describing: displayName.takeRetainedValue())
                    let lifetime = GSSCredentialGetLifetime(cred)
                    let expiretime = Date().addingTimeInterval(TimeInterval(lifetime))
                    
                    self.tickets[displayNameString]?.expired = false
                    self.tickets[displayNameString]?.expires = expiretime
                    self.tickets[displayNameString]?.GSSItem = cred
                    
                    if self.defaultPrincipal == displayNameString {
                        self.defaultExpires = expiretime
                        let newTicket = KerbTicket(principal: displayNameString, expiration: expiretime, defaultCache: true)
                        kerbTickets.append(newTicket)
                    } else {
                        let newTicket = KerbTicket(principal: displayNameString, expiration: expiretime, defaultCache: false)
                        kerbTickets.append(newTicket)
                    }
                    self.logger.debug("Possible Ticket: \(self.tickets.keys.joined(separator: ", "), privacy: .public) for account \(displayNameString, privacy: .public)")
                } else {
                    self.logger.debug("Expired credential - ignoring.")
                    var tempCred: gss_cred_id_t? = cred
                    gss_destroy_cred(&min_stat1, &tempCred)
                }
            }
        })
        
        // Clean up any expired tickets
        let ticks = tickets
        tickets.removeAll()
        
        for tick in ticks {
            if !tick.value.expired {
                tickets[tick.value.principal] = tick.value
            } else {
                _ = try? await cliTask("/usr/bin/kdestroy -p " + tick.value.principal)
            }
        }
        return kerbTickets
    }

    /// Checks if there are tickets for the specified principal
    public func hasTickets(principal: String) async -> Bool {
        await klist()
        return tickets.keys
            .map { $0.lowercased() }
            .contains(principal.lowercased())
    }

    /// Deletes a Kerberos ticket
    public func kdestroy(princ: String = "") {
        let name = princ.isEmpty ? defaultPrincipal! : princ
        logger.debug("Destroying ticket for: \(princ, privacy: .public)")
        
        var context: krb5_context? = nil
        krb5_init_secure_context(&context)
        
        krb5_cc_destroy(context, tickets[name]?.krb5Cache)
    }
    
    /// Switches the default cache
    public func kswitch(princ: String = "") {
        let name = princ.isEmpty ? defaultPrincipal! : princ
        var p: krb5_principal? = nil
        var cache: krb5_ccache? = nil
        
        logger.debug("Switching ticket for: \(princ, privacy: .public)")
        
        var context: krb5_context? = nil
        krb5_init_secure_context(&context)
        
        var nameInt = Int8(name)
        krb5_parse_name(context!, &nameInt!, &p)
        krb5_cc_cache_match(context, p, &cache)
    }
}
