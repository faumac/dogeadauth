//
//  ADUser.swift
//  dogeADAuth
//
//  Created by Joel Rennich on 9/9/17.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 13.12.23.
//  Copyright © 2023 Regionales Rechenzentrum Erlangen. All rights reserved.
//

import Foundation
import dogeADAuthObjC
import OSLog

public protocol dogeADUserSession {
//    func getKerberosTicket(principal: String?, completion: @escaping (KerberosTicketResult) -> Void)
    func getKerberosTicket(principal: String?) async -> KerberosTicketResult
    func authenticate(authTestOnly: Bool) async
    func changePassword(oldPassword: String, newPassword: String, completion: @escaping (String?) -> Void)
    func changePassword() async
    func userInfo() async
    var delegate: dogeADUserSessionDelegate? { get set }
    var state: dogeADSessionState { get }
}

public typealias KerberosTicketResult = Result<ADUserRecord, dogeADSessionError>

public protocol dogeADUserSessionDelegate: AnyObject {
    func dogeADAuthenticationSucceded() async
    func dogeADAuthenticationFailed(error: dogeADSessionError, description: String) async
    func dogeADUserInformation(user: ADUserRecord) async
}

public enum dogeADSessionState: CustomStringConvertible {
    case success
    case offDomain
    case siteFailure
    case networkLookup
    case passwordChangeRequired
    case unset
    case lookupError
    case kerbError
    
    public var description: String {
        switch self {
        case .success: return "success"
        case .offDomain: return "offDomain"
        case .siteFailure: return "siteFailure"
        case .networkLookup: return "networkLookup"
        case .passwordChangeRequired: return "passwordChangeRequired"
        case .unset: return "unset"
        case .lookupError: return "lookupError"
        case .kerbError: return "kerbError"
        }
    }
}

public enum dogeADSessionError: String, Error {
    case OffDomain
    case UnAuthenticated
    case SiteError
    case StateError
    case AuthenticationFailure
    case KerbError
    case PasswordExpired = "Password has expired"
    case unknownPrincipal
    case wrongRealm = "Wrong realm"
}

public enum LDAPType {
    case AD
    case OD
}

public struct dogeADLDAPServer {
    var host: String
    var status: String
    var priority: Int
    var weight: Int
    var timeStamp: Date
}

// MARK: Start of public class

/// A general purpose class that is the main entrypoint for interactions with Active Directory.
public class dogeADSession: NSObject {

    public var state: dogeADSessionState = .offDomain          // current state of affairs
    weak public var delegate: dogeADUserSessionDelegate?       // delegate
    public var site: String = ""                              // current AD site
    public var defaultNamingContext: String = ""              // current default naming context
    private var hosts = [dogeADLDAPServer]()                   // list of LDAP servers
    private var resolver = DNSResolver()                      // DNS resolver object
    private var maxSSF = ""                                   // current security level in place for LDAP lookups
    private var URIPrefix = "ldap://"                         // LDAP or LDAPS
    private var current = 0                                   // current LDAP server from hosts
    public var home = ""                                      // current active user home
    public var ldapServers: [String]?                         // static DCs to use instead of looking up via DNS records

    // Base configuration prefs
    // change these on the object as needed
    
    public var domain: String = ""                  // current LDAP Domain - can be set with init
    public var kerberosRealm: String = ""           // Kerberos realm
    public var createKerbPrefs: Bool = true         // Determines if skeleton Kerb prefs should be set
    
    public var siteIgnore: Bool = false             // ignore site lookup?
    public var siteForce: Bool = false              // force a site?
    public var siteForceSite: String = ""           // what site to force
    
    public var ldaptype: LDAPType = .AD             // Type of LDAP server
    public var port: Int = 389                      // LDAP port typically either 389 or 636
    public var anonymous: Bool = false              // Anonymous LDAP lookup
    public var useSSL: Bool = false                 // Toggle SSL
    
    public var recursiveGroupLookup : Bool = false  // Toggle recursive group lookup
    
    // User

    public var userPrincipal: String = ""           // Full user principal
    public var userPrincipalShort: String = ""      // user shortname - necessary for any lookups to happen
    public var userRecord: ADUserRecord? = nil      // ADUserRecordObject containing all user information
    public var userPass: String = ""                // for auth
    public var oldPass: String = ""                 // for password changes
    public var newPass: String = ""                 // for password changes
    public var customAttributes : [String]?

    let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "dogeADSession")
    
    // conv. init with domain and user
    
    /// Convenience initializer to create a `dogeADSession` with the given domain, username, and `LDAPType`.
    ///
    /// - Parameters:
    ///   - domain: The AD domain for the user.
    ///   - user: The user's name. Either the User Principal Short or the User Principal Name including the @domain syntax are accepted.
    ///   - type: The type of LDAP connection. Defaults to AD.
    public init(domain: String, user: String, type: LDAPType = .AD) {
        // Configuration parts
        self.domain = domain
        self.ldaptype = type

        // Check for the REALM
        if let atIndex = user.firstIndex(of: "@") {
            self.userPrincipalShort = String(user[..<atIndex])
            self.kerberosRealm = String(user[user.index(after: atIndex)...]).uppercased()
            self.userPrincipal = user
        } else {
            self.userPrincipalShort = user
            self.kerberosRealm = domain.uppercased()
            self.userPrincipal = "\(user)@\(self.kerberosRealm)"
        }
    }

    // MARK: conv functions
    
    // Return the current server
    
    var currentServer: String {
        // logging for current state seems to be unnecessary
        // logger.debug("Computed currentServer accessed in state: \(String(describing: self.state), privacy: .public)")

        if state != .offDomain {

            if self.hosts.isEmpty {
                logger.debug("Make sure we have LDAP servers")
                getHosts(for: domain)
            }
            // logging is too verbose
            // logger.debug("Lookup the current LDAP host in: \(String(describing: self.hosts), privacy: .public)")

            return self.hosts[current].host
        } else {
            return ""
        }
    }
    
    // MARK: DNS Main
    
    /// Parses the SRV reply and updates the results array with the target records.
    ///
    /// - Parameter results: An inout array of strings to store the parsed SRV targets.
    fileprivate func parseSRVReply(into results: inout [String]) {
        // Check for resolver error
        if let error = resolver.error {
            logger.error("Query Error: \(error.localizedDescription, privacy: .public)")
            return
        }
        
        // Ensure query results are available and of the expected type
        guard let records = resolver.queryResults as? [[String: Any]] else {
            logger.error("Unexpected query results format.")
            return
        }
        
        // Extract target records
        results = records.compactMap { $0["target"] as? String }
        
//        logger.debug("Parsed SRV targets: \(results, privacy: .public)")
    }
    
    /// Retrieves SRV records for a given domain and service type.
    ///
    /// - Parameters:
    ///   - domain: The domain for which to retrieve SRV records.
    ///   - srvType: The type of SRV service to query. Defaults to "_ldap._tcp.".
    /// - Returns: An array of SRV record strings.
    func getSRVRecords(for domain: String, srvType: String = "_ldap._tcp.") -> [String] {
        // Configure the resolver for SRV query
        resolver.queryType = "SRV"
        let queryValue = srvType + (site.isEmpty || srvType.contains("_kpasswd") ? domain : "\(site)._sites.\(domain)")
        resolver.queryValue = queryValue
        
        var results: [String] = []
        
        logger.debug("Starting DNS query for SRV records with query value: \(queryValue)")
        
        // Start the DNS query
        resolver.startQuery()
        
        // Run the loop until the query is finished
        while !resolver.finished {
            RunLoop.current.run(mode: .default, before: .distantFuture)
        }
        
        // Parse the SRV reply into results
        parseSRVReply(into: &results)
        
        logger.debug("Returning results: \(results, privacy: .public)")
        return results
    }

    
    /// Parses the host reply from the resolver and updates the hosts list accordingly.
    fileprivate func parseHostsReply() {
        // Check for resolver error
        if let error = resolver.error {
            logger.error("Query Error: \(error.localizedDescription, privacy: .public)")
            state = .siteFailure
            hosts.removeAll()
            return
        }
        
        // Ensure query results are available and of the expected type
        guard let records = resolver.queryResults as? [[String: Any]] else {
            logger.error("Unexpected query results format.")
            state = .siteFailure
            hosts.removeAll()
            return
        }
        
        logger.debug("Did Receive Query Result: \(self.resolver.queryResults.description, privacy: .public)")
        
        // Extract and sort new hosts
        let newHosts = records.compactMap { record -> dogeADLDAPServer? in
            guard let host = record["target"] as? String,
                  let priority = record["priority"] as? Int,
                  let weight = record["weight"] as? Int else {
                return nil
            }
            return dogeADLDAPServer(host: host, status: "found", priority: priority, weight: weight, timeStamp: Date())
        }.sorted { $0.priority < $1.priority }
        
        // Append fallback hosts
        hosts = newHosts + hosts
        state = .success
    }

    /// Retrieves the LDAP hosts for a given domain.
    ///
    /// - Parameter domain: The domain to query for LDAP hosts.
    fileprivate func getHosts(for domain: String) {
        // Check if static LDAP servers are available
        if let servers = ldapServers {
            logger.debug("Using static DC list.")
            
            // Map static servers to dogeADLDAPServer objects
            hosts = servers.map { server in
                dogeADLDAPServer(host: server, status: "found", priority: 100, weight: 100, timeStamp: Date())
            }.sorted { $0.priority < $1.priority }
            
            state = .success
            site = "STATIC"
            return
        }
        
        // Configure the resolver for SRV query
        resolver.queryType = "SRV"
        resolver.queryValue = site.isEmpty ? "_ldap._tcp.\(domain)" : "_ldap._tcp.\(site)._sites.\(domain)"
        
        logger.debug("Starting DNS query for SRV records with query value: \(self.resolver.queryValue)")
        
        // Start the DNS query
        resolver.startQuery()
        
        // Run the loop until the query is finished
        while !resolver.finished {
            RunLoop.current.run(mode: .default, before: .distantFuture)
            logger.debug("Waiting for DNS query to return.")
        }
        
        // Parse the results from the DNS query
        parseHostsReply()
    }
    
    /// Tests the connectivity of each host in the `hosts` array asynchronously.
    func testHosts() async {
        // Ensure the state is successful before proceeding
        guard state == .success else {
            logger.debug("State is not success; skipping host tests.")
            return
        }
        
        // Use a task group to test each host concurrently
        await withTaskGroup(of: Void.self) { group in
            for (index, host) in hosts.enumerated() {
                group.addTask { [self] in
                    logger.debug("Testing host: \(host.host)")
                    await self.testSingleHost(at: index)
                }
            }
        }
        
        logger.debug("Completed testing all hosts.")
    }


    /// Tests the connectivity of a single host at the specified index.
    ///
    /// - Parameter index: The index of the host in the `hosts` array to test.
    private func testSingleHost(at index: Int) async {
        // Ensure the host is not already marked as "dead"
        guard hosts[index].status != "dead" else {
            logger.debug("Skipping host at index \(index) as it is marked dead.")
            return
        }
        
        let host = hosts[index].host
        logger.info("Trying host: \(host, privacy: .public)")
        
        // Execute the network check command
        let socketResult = await cliTask("/usr/bin/nc -G 5 -z \(host) \(port)")
        
        // Check if the connection succeeded
        if socketResult.contains("succeeded!") {
            logger.debug("Connection succeeded for host: \(host)")
            await testLDAP(for: index)
        } else {
            logger.debug("Connection failed for host: \(host)")
            await markHostAsDead(index)
        }
    }

    /// Tests the LDAP connection for the host at the specified index.
    ///
    /// - Parameter index: The index of the host to test.
    private func testLDAP(for index: Int) async {
        let attribute = (ldaptype == .OD) ? "namingContexts" : "defaultNamingContext"
        let command = "/usr/bin/ldapsearch -N -LLL \(anonymous ? "-x" : "-Q") \(maxSSF)-l 3 -s base -H \(URIPrefix)\(hosts[index].host):\(port) \(attribute)"
        
        do {
            // Execute the LDAP search command
            let ldapResult = try await cliTask(command)
            
            // Check if the result is valid and does not contain errors
            guard !ldapResult.isEmpty,
                  !ldapResult.contains("GSSAPI Error"),
                  !ldapResult.contains("Can't contact") else {
                logger.error("LDAP search failed for host: \(self.hosts[index].host, privacy: .public)")
                await markHostAsDead(index)
                return
            }
            
            // Clean the LDIF result
            let ldifResult = cleanLDIF(ldapResult)
            
            // Ensure the cleaned result is not empty
            guard !ldifResult.isEmpty else {
                logger.error("Cleaned LDIF is empty for host: \(self.hosts[index].host, privacy: .public)")
                await markHostAsDead(index)
                return
            }
            
            // Update the host status and default naming context on the main thread
            await MainActor.run {
                self.defaultNamingContext = self.getAttributeForSingleRecordFromCleanedLDIF(attribute, ldif: ldifResult)
                self.hosts[index].status = "live"
                self.hosts[index].timeStamp = Date()
                self.current = index
            }
            
            logger.info("Current LDAP Server is: \(self.hosts[index].host, privacy: .public)")
            logger.info("Current default naming context: \(self.defaultNamingContext, privacy: .public)")
            
        } catch {
            logger.error("Error executing LDAP search for host: \(self.hosts[index].host, privacy: .public) - \(error.localizedDescription, privacy: .public)")
            await markHostAsDead(index)
        }
    }

    /// Updates the status and timestamp of a host.
    ///
    /// - Parameters:
    ///   - host: The `dogeADLDAPServer` whose status needs to be updated.
    ///   - status: The new status to set for the host.
    private func updateHostStatus(for host: dogeADLDAPServer, status: String) async {
        await MainActor.run {
            if let index = hosts.firstIndex(where: { $0.host == host.host }) {
                hosts[index].status = status
                hosts[index].timeStamp = Date()
                logger.info("Updated host status: \(host.host, privacy: .public) to \(status)")
            } else {
                logger.error("Host not found: \(host.host, privacy: .public)")
            }
        }
    }

    /// Marks the host at the specified index as dead.
    ///
    /// - Parameter index: The index of the host to mark as dead.
    private func markHostAsDead(_ index: Int) async {
        await updateHostStatus(for: hosts[index], status: "dead")
    }



    
    // MARK: Sites
    
    // private function to get the AD site
    
    /// Finds the site by querying LDAP and updates the site information accordingly.
    fileprivate func findSite() async {
        let tempDefaultNamingContext = defaultNamingContext
        defaultNamingContext = ""
        
        let attribute = "netlogon"
        let searchTerm = "(&(DnsDomain=\(domain))(NtVer=\\06\\00\\00\\00))"
        
        // Perform LDAP query
        guard let ldifResult = try? await getLDAPInformation(
            [attribute],
            baseSearch: true,
            searchTerm: searchTerm,
            test: false,
            overrideDefaultNamingContext: true
        ) else {
            logger.info("LDAP Query failed.")
            resetDefaultNamingContext(to: tempDefaultNamingContext)
            return
        }
        
        // Extract and validate LDAP ping information
        let ldapPingBase64 = getAttributeForSingleRecordFromCleanedLDIF(attribute, ldif: ldifResult)
            .trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard !ldapPingBase64.isEmpty, let ldapPing = ADLDAPPing(ldapPingBase64String: ldapPingBase64) else {
            logger.info("Invalid ldapPingBase64.")
            resetDefaultNamingContext(to: tempDefaultNamingContext)
            return
        }
        
        // Determine the site based on preferences and LDAP ping
        site = determineSite(using: ldapPing)
        
        // Handle site and server information
        if ldapPing.flags.contains(.DS_CLOSEST_FLAG) {
            logger.info("The current server is the closest server.")
        } else if !site.isEmpty {
            logger.info("Site \(self.site, privacy: .public) found.")
            logger.info("Looking up DCs for site.")
            
            let currentHosts = hosts
            getHosts(for: domain)
            
            if hosts.isEmpty || hosts[0].host.isEmpty {
                logger.info("Site \(self.site, privacy: .public) has no DCs configured. Ignoring site. You should fix this.")
                hosts = currentHosts
            }
            
            await testHosts()
        } else {
            logger.info("Unable to find site.")
        }
        
        resetDefaultNamingContext(to: tempDefaultNamingContext)
    }

    /// Resets the default naming context to the specified value.
    ///
    /// - Parameter context: The context to reset to.
    private func resetDefaultNamingContext(to context: String) {
        logger.debug("Resetting default naming context to: \(context, privacy: .public)")
        defaultNamingContext = context
    }

    /// Determines the site based on preferences and LDAP ping information.
    ///
    /// - Parameter ldapPing: The LDAP ping information.
    /// - Returns: The determined site.
    private func determineSite(using ldapPing: ADLDAPPing) -> String {
        if siteIgnore {
            logger.debug("Sites being ignored due to preferences.")
            return ""
        } else if siteForce {
            logger.debug("Site being forced to site set in preferences.")
            return siteForceSite
        } else {
            return ldapPing.clientSite
        }
    }

    
    // MARK: LDAP Retrieval
    
    /// Retrieves LDAP information based on the specified parameters.
    ///
    /// - Parameters:
    ///   - attributes: The list of attributes to retrieve.
    ///   - baseSearch: Whether to perform a base search.
    ///   - searchTerm: The search term to use.
    ///   - test: Whether to test the socket connection before querying.
    ///   - overrideDefaultNamingContext: Whether to override the default naming context.
    /// - Returns: An array of dictionaries containing the LDAP information.
    /// - Throws: `dogeADSessionError.StateError` if the connection or query fails.
    func getLDAPInformation(
        _ attributes: [String],
        baseSearch: Bool = false,
        searchTerm: String = "",
        test: Bool = true,
        overrideDefaultNamingContext: Bool = false
    ) async throws -> [[String: String]] {
        
        // Test the socket connection if required
        if test {
            guard await testSocket(currentServer) else {
                throw dogeADSessionError.StateError
            }
        }
        
        // Test hosts if default naming context is invalid and not overridden
        if !overrideDefaultNamingContext {
            if (defaultNamingContext.isEmpty || defaultNamingContext.contains("GSSAPI Error")) && !hosts.isEmpty {
                await testHosts()
            }
        }
        
        // Prepare the LDAP search command and arguments
        let command = "/usr/bin/ldapsearch"
        var arguments: [String] = ["-N", anonymous ? "-x" : "-Q", "-LLL", "-o", "nettimeout=1", "-o", "ldif-wrap=no"]
        
        if baseSearch {
            arguments.append(contentsOf: ["-s", "base"])
        }
        
        if !maxSSF.isEmpty {
            arguments.append(contentsOf: ["-O", "maxssf=0"])
        }
        
        arguments.append(contentsOf: ["-H", "\(URIPrefix)\(currentServer)", "-b", defaultNamingContext])
        
        if !searchTerm.isEmpty {
            arguments.append(searchTerm)
        }
        
        arguments.append(contentsOf: attributes)
        
        // Execute the LDAP search command
        let ldapResult: String
        do {
            ldapResult = try await dogeADAuth.cliTask(command, arguments: arguments)
        } catch {
            logger.error("LDAP command execution failed: \(error.localizedDescription, privacy: .public)")
            throw dogeADSessionError.StateError
        }
        
        // Check for errors in the LDAP result
        guard !ldapResult.contains("GSSAPI Error"), !ldapResult.contains("Can't contact") else {
            logger.error("LDAP query failed with error in result.")
            throw dogeADSessionError.StateError
        }
        
        // Clean and return the LDIF result
        let myResult = cleanLDIF(ldapResult)
        return myResult
    }


    
    /// Cleans and extracts group names from a semicolon-separated string.
    ///
    /// - Parameter groupsTemp: A semicolon-separated string of group identifiers.
    /// - Returns: An array of cleaned group names.
    fileprivate func cleanGroups(_ groupsTemp: String?) -> [String] {
        // Ensure the input string is not nil
        guard let groupsTemp = groupsTemp else {
            return []
        }
        
        // Split the input string by semicolons and clean each group name
        return groupsTemp
            .components(separatedBy: ";")
            .compactMap { group in
                // Split each group by commas and take the first component
                guard let firstComponent = group.components(separatedBy: ",").first else {
                    return nil
                }
                
                // Remove "CN=" or "cn=" prefix and trim whitespace
                let cleanedGroup = firstComponent
                    .replacingOccurrences(of: "CN=", with: "")
                    .replacingOccurrences(of: "cn=", with: "")
                    .trimmingCharacters(in: .whitespacesAndNewlines)
                
                // Return the cleaned group name if it's not empty
                return cleanedGroup.isEmpty ? nil : cleanedGroup
            }
    }


    /// Looks up recursive groups for a given distinguished name (DN).
    ///
    /// - Parameter dn: The distinguished name to search for.
    /// - Returns: A semicolon-separated string of group DNs, or nil if none are found or lookup is disabled.
    fileprivate func lookupRecursiveGroups(_ dn: String) async -> String? {
        // Check if recursive group lookup is enabled
        guard recursiveGroupLookup else {
            return nil
        }
        
        // Define the LDAP attributes and search term
        let attributes = ["name"]
        let searchTerm = "(member:1.2.840.113556.1.4.1941:=\(dn.replacingOccurrences(of: "\\", with: "\\\\5c")))"
        
        // Perform the LDAP query
        guard let ldifResult = try? await getLDAPInformation(attributes, searchTerm: searchTerm) else {
            logger.error("Failed to retrieve LDAP information for DN: \(dn, privacy: .public)")
            return nil
        }
        
        // Extract and join group DNs
        let groups = ldifResult.compactMap { item in
            item["dn"]
        }.joined(separator: ";")
        
        // Return the group DNs or nil if empty
        return groups.isEmpty ? nil : groups
    }



    /// Parses the expiration date for a user's password based on various parameters.
    ///
    /// - Parameters:
    ///   - computedExpireDateRaw: The raw computed expiration date as a string.
    ///   - userPasswordUACFlag: The user account control flag as a string.
    ///   - tempPasswordSetDate: The temporary password set date.
    /// - Returns: A tuple containing password aging status, user password expiration date, and server password expiration default.
    fileprivate func parseExpirationDate(
        _ computedExpireDateRaw: String?,
        _ userPasswordUACFlag: String,
        _ tempPasswordSetDate: Date
    ) async -> (passwordAging: Bool, userPasswordExpireDate: Date, serverPasswordExpirationDefault: Double) {
        
        var passwordAging = false
        var userPasswordExpireDate = Date()
        var serverPasswordExpirationDefault = 0.0
        
        if let computedExpireDateRaw = computedExpireDateRaw, let computedExpireDate = Int(computedExpireDateRaw) {
            switch computedExpireDate {
            case Int.max:
                passwordAging = false
                userPasswordExpireDate = .distantFuture
            case 0:
                passwordAging = true
                userPasswordExpireDate = .distantPast
            default:
                passwordAging = true
                userPasswordExpireDate = Date(timeIntervalSince1970: Double(computedExpireDate) / 10000000 - 11644473600)
            }
        } else {
            let attribute = "maxPwdAge"
            do {
                let ldifResult = try await getLDAPInformation([attribute], baseSearch: true)
                let passwordExpirationLength = getAttributeForSingleRecordFromCleanedLDIF(attribute, ldif: ldifResult)
                
                if passwordExpirationLength.count > 15 {
                    passwordAging = false
                } else if !passwordExpirationLength.isEmpty, let userPasswordUACFlagInt = Int(userPasswordUACFlag) {
                    passwordAging = userPasswordUACFlagInt & 0x10000 == 0
                    if let expirationValue = Int(passwordExpirationLength) {
                        serverPasswordExpirationDefault = passwordAging ? abs(Double(expirationValue) / 10000000) : 0.0
                    }
                }
                
                userPasswordExpireDate = tempPasswordSetDate.addingTimeInterval(serverPasswordExpirationDefault)
            } catch {
                logger.error("Failed to retrieve LDAP information for maxPwdAge: \(error.localizedDescription, privacy: .public)")
            }
        }
        
        return (passwordAging, userPasswordExpireDate, serverPasswordExpirationDefault)
    }



    /// Extracts user attributes from an LDAP search.
    ///
    /// - Parameters:
    ///   - attributes: The list of attributes to retrieve.
    ///   - searchTerm: The search term to use.
    /// - Returns: A tuple containing the user's home directory, display name, memberOf, mail, and uid.
    fileprivate func extractedFunc(
        _ attributes: [String],
        _ searchTerm: String
    ) async -> (homeDirectory: String, displayName: String, memberOf: String?, mail: String, uid: String) {
        
        // Perform the LDAP query
        guard let ldifResult = try? await getLDAPInformation(attributes, searchTerm: searchTerm) else {
            logger.info("Unable to find user.")
            return ("", "", nil, "", "")
        }
        
        // Extract attributes from the LDAP result
        let ldapResult = getAttributesForSingleRecordFromCleanedLDIF(attributes, ldif: ldifResult)
        
        // Retrieve individual attributes with default values
        let homeDirectory = ldapResult["homeDirectory"] ?? ""
        let displayName = ldapResult["displayName"] ?? ""
        let memberOf = ldapResult["memberOf"]
        let mail = ldapResult["mail"] ?? ""
        let uid = ldapResult["uid"] ?? ""
        
        return (homeDirectory, displayName, memberOf, mail, uid)
    }

    /// Retrieves user information from LDAP and updates the user record.
    func getUserInformation() async {
        var passwordAging = true
        var tempPasswordSetDate = Date()
        var serverPasswordExpirationDefault = 0.0
        var userPasswordExpireDate = Date()
        var groups = [String]()
        var userHome = ""

        var attributes: [String]
        let searchTerm: String

        // Determine attributes and search term based on LDAP type
        if ldaptype == .AD {
            attributes = [
                "pwdLastSet", "msDS-UserPasswordExpiryTimeComputed", "userAccountControl",
                "homeDirectory", "displayName", "memberOf", "mail", "userPrincipalName",
                "dn", "givenName", "sn", "cn", "msDS-ResultantPSO", "msDS-PrincipalName"
            ]

            if let customAttributes = customAttributes, !customAttributes.isEmpty {
                attributes.append(contentsOf: customAttributes)
            }

            searchTerm = "sAMAccountName=\(userPrincipalShort)"
        } else {
            attributes = ["homeDirectory", "displayName", "memberOf", "mail", "uid"]
            searchTerm = "uid=\(userPrincipalShort)"
        }

        // Perform LDAP query
        guard let ldifResult = try? await getLDAPInformation(attributes, searchTerm: searchTerm) else {
            logger.info("Unable to find user.")
            return
        }

        let ldapResult = getAttributesForSingleRecordFromCleanedLDIF(attributes, ldif: ldifResult)

        // Process LDAP results based on LDAP type
        if ldaptype == .AD {
            // Extract and process AD-specific attributes
            let passwordSetDate = ldapResult["pwdLastSet"]
            let computedExpireDateRaw = ldapResult["msDS-UserPasswordExpiryTimeComputed"]
            let userPasswordUACFlag = ldapResult["userAccountControl"] ?? ""
            let userHomeTemp = ldapResult["homeDirectory"] ?? ""
            let userDisplayName = ldapResult["displayName"] ?? ""
            let firstName = ldapResult["givenName"] ?? ""
            let lastName = ldapResult["sn"] ?? ""
            var groupsTemp = ldapResult["memberOf"]
            let userEmail = ldapResult["mail"] ?? ""
            let upn = ldapResult["userPrincipalName"] ?? ""
            let dn = ldapResult["dn"] ?? ""
            let cn = ldapResult["cn"] ?? ""
            let pso = ldapResult["msDS-ResultantPSO"] ?? ""
            let ntName = ldapResult["msDS-PrincipalName"] ?? ""

            var customAttributeResults: [String: Any]?

            if let customAttributes = customAttributes, !customAttributes.isEmpty {
                customAttributeResults = customAttributes.reduce(into: [String: Any]()) { result, key in
                    result[key] = ldapResult[key] ?? ""
                }
            }

            if !ldapResult.isEmpty {
                groupsTemp = await lookupRecursiveGroups(dn)

                if let passwordSetDate = passwordSetDate, let passwordSetDateDouble = Double(passwordSetDate) {
                    tempPasswordSetDate = Date(timeIntervalSince1970: passwordSetDateDouble / 10000000 - 11644473600)
                }

                let (passwordAgingResult, userPasswordExpireDateResult, serverPasswordExpirationDefaultResult) = await parseExpirationDate(computedExpireDateRaw, userPasswordUACFlag, tempPasswordSetDate)
                passwordAging = passwordAgingResult
                userPasswordExpireDate = userPasswordExpireDateResult
                serverPasswordExpirationDefault = serverPasswordExpirationDefaultResult

                groups = cleanGroups(groupsTemp)

                userHome = userHomeTemp.replacingOccurrences(of: "\\", with: "/").replacingOccurrences(of: " ", with: "%20")

                userRecord = await ADUserRecord(
                    userPrincipal: userPrincipal,
                    firstName: firstName,
                    lastName: lastName,
                    fullName: userDisplayName,
                    shortName: userPrincipalShort,
                    upn: upn,
                    email: userEmail,
                    groups: groups,
                    homeDirectory: userHome,
                    passwordSet: tempPasswordSetDate,
                    passwordExpire: userPasswordExpireDate,
                    uacFlags: Int(userPasswordUACFlag),
                    passwordAging: passwordAging,
                    computedExpireDate: userPasswordExpireDate,
                    updatedLast: Date(),
                    domain: domain,
                    cn: cn,
                    pso: pso,
                    passwordLength: getComplexity(pso: pso),
                    ntName: ntName,
                    customAttributes: customAttributeResults
                )
            }
        } else {
            // Process Open Directory-specific attributes
            let (homeDirectory, displayName, memberOf, mail, uid) = await extractedFunc(attributes, searchTerm)
            userHome = homeDirectory
            groups = cleanGroups(memberOf)

            userRecord = ADUserRecord(
                userPrincipal: userPrincipal,
                firstName: "",
                lastName: "",
                fullName: displayName,
                shortName: userPrincipalShort,
                upn: "",
                email: mail,
                groups: groups,
                homeDirectory: userHome,
                passwordSet: tempPasswordSetDate,
                passwordExpire: userPasswordExpireDate,
                uacFlags: 0,
                passwordAging: passwordAging,
                computedExpireDate: userPasswordExpireDate,
                updatedLast: Date(),
                domain: domain,
                cn: "",
                pso: "",
                passwordLength: 0,
                ntName: uid,
                customAttributes: nil
            )
        }
    }



    
    // MARK: LDAP cleanup functions
    
    /// Cleans and parses an LDIF string into an array of dictionaries representing records.
    ///
    /// - Parameter ldif: The LDIF string to parse.
    /// - Returns: An array of dictionaries, each representing a record with attribute-value pairs.
    fileprivate func cleanLDIF(_ ldif: String) -> [[String: String]] {
        let ldifLines = ldif.components(separatedBy: .newlines)
        var records = [[String: String]]()
        var record = [String: String]()
        
        var i = 0
        while i < ldifLines.count {
            var line = ldifLines[i].trimmingCharacters(in: .whitespaces)
            
            // Skip version and comments
            if (i == 0 && line.hasPrefix("version")) || line.hasPrefix("#") {
                i += 1
                continue
            }
            
            // Fold lines
            while i + 1 < ldifLines.count && ldifLines[i + 1].hasPrefix(" ") {
                line += ldifLines[i + 1].trimmingCharacters(in: .whitespaces)
                i += 1
            }
            
            if line.isEmpty {
                // End of record
                if !record.isEmpty {
                    records.append(record)
                    record = [String: String]()
                }
            } else {
                let attributeParts = line.split(separator: ":", maxSplits: 1, omittingEmptySubsequences: false)
                if attributeParts.count == 2 {
                    var attributeName = String(attributeParts[0]).trimmingCharacters(in: .whitespaces)
                    if let index = attributeName.firstIndex(of: ";") {
                        attributeName = String(attributeName[..<index])
                    }
                    
                    var attributeValue = String(attributeParts[1]).trimmingCharacters(in: .whitespaces)
                    if attributeValue.hasPrefix("<") {
                        attributeValue = String(attributeValue.dropFirst()).trimmingCharacters(in: .whitespaces)
                    } else if attributeValue.hasPrefix(":") {
                        let base64Value = String(attributeValue.dropFirst()).trimmingCharacters(in: .whitespaces)
                        if let data = Data(base64Encoded: base64Value) {
                            attributeValue = String(data: data, encoding: .utf8) ?? ""
                        } else {
                            attributeValue = ""
                        }
                    }
                    
                    attributeValue = attributeValue.replacingOccurrences(of: "\"", with: "\"\"")
                    
                    if let existingValue = record[attributeName] {
                        record[attributeName] = existingValue + ";" + attributeValue
                    } else {
                        record[attributeName] = attributeValue
                    }
                }
            }
            
            i += 1
        }
        
        // Save last record
        if !record.isEmpty {
            records.append(record)
        }
        
        return records
    }

    /// Retrieves a single attribute value from the first record in the cleaned LDIF.
    ///
    /// - Parameters:
    ///   - attribute: The attribute to retrieve.
    ///   - ldif: The cleaned LDIF records.
    /// - Returns: The value of the attribute, or an empty string if not found.
    fileprivate func getAttributeForSingleRecordFromCleanedLDIF(_ attribute: String, ldif: [[String: String]]) -> String {
        return ldif.lazy.compactMap { $0[attribute] }.first ?? ""
    }

    /// Retrieves specified attributes from the first matching record in the cleaned LDIF.
    ///
    /// - Parameters:
    ///   - attributes: The list of attributes to retrieve.
    ///   - ldif: The cleaned LDIF records.
    /// - Returns: A dictionary of attribute-value pairs for the first matching record.
    fileprivate func getAttributesForSingleRecordFromCleanedLDIF(_ attributes: [String], ldif: [[String: String]]) -> [String: String] {
        for record in ldif {
            let filteredRecord = record.filter { attributes.contains($0.key) }
            if !filteredRecord.isEmpty {
                return filteredRecord
            }
        }
        return [:]
    }
 
    /// Tests the socket connection to a specified host.
    ///
    /// - Parameter host: The host to test the socket connection.
    /// - Returns: A Boolean indicating whether the socket connection was successful.
    fileprivate func testSocket(_ host: String) async -> Bool {
        // Verwende String-Interpolation für Klarheit und einfache Handhabung
        let command = "/usr/bin/nc -G 5 -z \(host) \(port)"
        
        do {
            let mySocketResult = try await cliTask(command)
            // Rückgabeziel optimieren, ohne 'if' zu benötigen
            return mySocketResult.contains("succeeded!")
        } catch {
            logger.error("Socket test failed for host \(host, privacy: .public): \(error.localizedDescription, privacy: .public)")
            return false
        }
    }
  
    /// Tests the LDAP connection to a specified host.
    ///
    /// - Parameter host: The host to test the LDAP connection.
    /// - Returns: A Boolean indicating whether the LDAP connection was successful.
    fileprivate func testLDAP(_ host: String) async -> Bool {
        let attribute = (ldaptype == .OD) ? "namingContexts" : "defaultNamingContext"
        
        // TODO: Implement swapPrincipals(false) if necessary
        
        // Construct the LDAP search command
        let command: String
        if anonymous {
            command = "/usr/bin/ldapsearch -N -LLL -x \(maxSSF)-l 3 -s base -H \(URIPrefix)\(host) \(attribute)"
        } else {
            command = "/usr/bin/ldapsearch -N -LLL -Q \(maxSSF)-l 3 -s base -H \(URIPrefix)\(host) \(attribute)"
        }
        
        do {
            let myLDAPResult = try await cliTask(command)
            
            // TODO: Implement swapPrincipals(true) if necessary
            
            // Check for errors in the LDAP result
            if !myLDAPResult.isEmpty && !myLDAPResult.contains("GSSAPI Error") && !myLDAPResult.contains("Can't contact") {
                let ldifResult = cleanLDIF(myLDAPResult)
                if !ldifResult.isEmpty {
                    defaultNamingContext = getAttributeForSingleRecordFromCleanedLDIF(attribute, ldif: ldifResult)
                    return true
                }
            }
        } catch {
            logger.error("LDAP test failed for host \(host, privacy: .public): \(error.localizedDescription, privacy: .public)")
        }
        
        return false
    }



    // MARK: Kerberos preference file needs to be updated:

    /// Checks for the presence of a Kerberos password server (kpasswd) that matches the current LDAP server.
    ///
    /// - Returns: A Boolean indicating whether the kpasswd server was successfully set.
    private func checkKpasswdServer() -> Bool {
        // Ensure we have LDAP servers
        if hosts.isEmpty {
            logger.debug("Make sure we have LDAP servers")
            getHosts(for: domain)
        }
        
        logger.debug("Searching for Kerberos SRV records")
        
        // Retrieve Kerberos password servers
        let kpasswdServers = getSRVRecords(for: domain, srvType: "_kpasswd._tcp.")
        logger.debug("New kpasswd Servers are: \(kpasswdServers.description, privacy: .public)")
        logger.debug("Current Server is: \(self.currentServer, privacy: .public)")
        
        // Check if the current server is among the Kerberos password servers
        guard kpasswdServers.contains(currentServer) else {
            logger.info("Couldn't find kpasswd server that matches current LDAP server. Letting system choose.")
            return false
        }
        
        logger.debug("Found kpasswd server that matches current LDAP server.")
        logger.debug("Attempting to set kpasswd server to ensure Kerberos and LDAP are in sync.")
        
        // Access Kerberos preferences
        let kerbPrefs = UserDefaults(suiteName: "com.apple.Kerberos")
        var kerbDefaults = kerbPrefs?.dictionary(forKey: "libdefaults") ?? [:]
        
        // Set default realm if not already set
        if kerbDefaults["default_realm"] == nil {
            kerbDefaults["default_realm"] = kerberosRealm
            kerbPrefs?.set(kerbDefaults, forKey: "libdefaults")
        } else {
            logger.debug("Existing default realm. Skipping adding default realm to Kerberos prefs.")
        }
        
        // Access Kerberos realms
        var kerbRealms = kerbPrefs?.dictionary(forKey: "realms") ?? [:]
        
        // Set kpasswd server for the realm if not already set
        if kerbRealms[kerberosRealm] != nil {
            logger.debug("Existing Kerberos configuration for realm. Skipping adding KDC to Kerberos prefs.")
            return false
        } else {
            kerbRealms[kerberosRealm] = ["kpasswd_server": currentServer]
            kerbPrefs?.set(kerbRealms, forKey: "realms")
            return true
        }
    }


    /// Calculates the password complexity based on the given Password Settings Object (PSO).
    ///
    /// - Parameter pso: The Password Settings Object. If empty, uses default settings.
    /// - Returns: An optional integer representing the password complexity, or nil if not found.
    fileprivate func getComplexity(pso: String = "") async -> Int? {
        let attributes: [String]
        let baseSearch: Bool
        let searchTerm: String
        
        // Determine attributes and search parameters based on PSO
        if pso.isEmpty {
            attributes = ["minPwdLength"]
            baseSearch = true
            searchTerm = ""
        } else {
            attributes = ["msDS-MinimumPasswordLength"]
            baseSearch = false
            searchTerm = "(objectClass=msDS-PasswordSettings)"
        }
        
        // Perform LDAP query
        guard let result = try? await getLDAPInformation(attributes, baseSearch: baseSearch, searchTerm: searchTerm, test: true, overrideDefaultNamingContext: false) else {
            return nil
        }
        
        // Extract the relevant attribute from the LDAP result
        let resultClean = getAttributesForSingleRecordFromCleanedLDIF(attributes, ldif: result)
        let attributeKey = pso.isEmpty ? "minPwdLength" : "msDS-MinimumPasswordLength"
        
        // Convert the attribute value to an integer
        guard let final = resultClean[attributeKey], let complexity = Int(final) else {
            return nil
        }
        
        return complexity
    }


    /// Removes a default realm from the Kerberos preferences file.
    ///
    /// - Parameter clearLibDefaults: A Boolean indicating whether to clear the library defaults.
    fileprivate func cleanKerbPrefs(clearLibDefaults: Bool = false) {
        guard let kerbPrefs = UserDefaults(suiteName: "com.apple.Kerberos") else {
            logger.error("Failed to access Kerberos preferences.")
            return
        }
        
        // Remove Kerberos realms
        var kerbRealms = kerbPrefs.dictionary(forKey: "realms") ?? [:]
        
        if kerbRealms[kerberosRealm] == nil {
            logger.debug("No realm in com.apple.Kerberos defaults.")
        } else {
            logger.debug("Removing realm from Kerberos Preferences.")
            kerbRealms.removeValue(forKey: kerberosRealm)
            kerbPrefs.set(kerbRealms, forKey: "realms")
            
            if clearLibDefaults {
                logger.debug("Clearing library defaults.")
                var libDefaults = kerbPrefs.dictionary(forKey: "libdefaults") ?? [:]
                libDefaults.removeValue(forKey: "default_realm")
                kerbPrefs.set(libDefaults, forKey: "libdefaults")
            }
        }
    }

    /// Creates a minimal Kerberos preferences file to prevent issues during password changes.
    ///
    /// - Parameter realm: The realm to set as the default. If nil, uses the current `kerberosRealm`.
    fileprivate func createBasicKerbPrefs(realm: String? = nil) {
        let realmToSet = realm ?? kerberosRealm
        
        guard let kerbPrefs = UserDefaults(suiteName: "com.apple.Kerberos") else {
            logger.error("Failed to access Kerberos preferences.")
            return
        }
        
        var kerbDefaults = kerbPrefs.dictionary(forKey: "libdefaults") ?? [:]
        
        if kerbDefaults["default_realm"] == nil {
            logger.debug("Setting default realm in Kerberos preferences.")
            kerbDefaults["default_realm"] = realmToSet
            kerbPrefs.set(kerbDefaults, forKey: "libdefaults")
        } else {
            logger.debug("Existing default realm. Skipping adding default realm to Kerberos prefs.")
        }
    }
}

extension dogeADSession: dogeADUserSession {
    /// Attempts to retrieve a Kerberos ticket for the specified principal.
    ///
    /// - Parameter principal: The principal for which to retrieve the ticket. Defaults to `nil`.
    /// - Returns: A `KerberosTicketResult` indicating success or failure.
    public func getKerberosTicket(principal: String? = nil) async -> KerberosTicketResult {
        // Check if the system already has tickets for the specified principal
        if let principal = principal, await klistUtil.hasTickets(principal: principal) {
            do {
                let userRecord = try await shareKerberosResult()
                return .success(userRecord)
            } catch {
                logger.debug("Error getting Kerberos ticket: \(error.localizedDescription).")
                return .failure(.KerbError)
            }
        }

        do {
            // Attempt to get Kerberos credentials
            try await KerbUtil().getKerberosCredentialsAsync(userPass, userPrincipal)
            self.userPass = "" // Clear the password after use
            return await processKerberosResult()
        } catch let error as dogeADSessionError {
            self.state = .kerbError
            return .failure(error)
        } catch {
            self.state = .kerbError
            let sessionError: dogeADSessionError
            
            // Determine the specific error type based on the error description
            switch error.localizedDescription {
            case dogeADSessionError.PasswordExpired.rawValue:
                sessionError = .PasswordExpired
            case dogeADSessionError.wrongRealm.rawValue:
                sessionError = .wrongRealm
            case let errorDesc where errorDesc.contains("unable to reach any KDC in realm"):
                sessionError = .OffDomain
            default:
                sessionError = .KerbError
            }
            
            return .failure(sessionError)
        }
    }


    /// Processes the result of a Kerberos authentication attempt.
    ///
    /// - Returns: A `KerberosTicketResult` indicating success or failure.
    private func processKerberosResult() async -> KerberosTicketResult {
        state = .offDomain

        // Retrieve the ticket
        await klistUtil.klist()

        // Check if the ticket is valid
        guard await klistUtil.returnDefaultPrincipal().contains(kerberosRealm) || anonymous else {
            return .failure(.UnAuthenticated)
        }

        // Configure for SSL if necessary
        if useSSL {
            URIPrefix = "ldaps://"
            port = 636
            maxSSF = "-O maxssf=0 "
        }

        // Manage hosts and server settings
        if let server = siteManager.sites[domain] {
            hosts = server
            state = .success
        } else {
            getHosts(for: domain)
            guard !hosts.isEmpty else {
                return .failure(.OffDomain)
            }
            siteManager.updateSites(for: domain, with: hosts)

            // Perform LDAP ping to determine the site
            if ldaptype == .AD {
                await findSite()
                guard state == .success else {
                    return .failure(.SiteError)
                }
            }
        }

        // Test hosts if available
        if !hosts.isEmpty {
            await testHosts()
        }

        do {
            let userRecord = try await shareKerberosResult()
            return .success(userRecord)
        } catch {
            logger.error("Failed to share Kerberos result: \(error.localizedDescription, privacy: .public)")
            return .failure(.KerbError)
        }
    }


    /// Retrieves and returns the user record after obtaining Kerberos information.
    ///
    /// - Throws: `dogeADSessionError.KerbError` if the user record cannot be retrieved.
    /// - Returns: An `ADUserRecord` containing the user's information.
    private func shareKerberosResult() async throws -> ADUserRecord {
        // Retrieve user information asynchronously
        await getUserInformation()
        
        // Ensure the user record is available
        guard let userRecord = userRecord else {
            logger.error("Failed to retrieve user record after Kerberos authentication.")
            throw dogeADSessionError.KerbError
        }
        
        return userRecord
    }


    /// Authenticates a user via Kerberos. If only looking to test the password without getting a ticket, set `authTestOnly` to true.
    ///
    /// Note: This will remove any pre-existing tickets for this user.
    ///
    /// - Parameter authTestOnly: Indicates if the authentication attempt should only validate the password without getting Kerberos tickets. Defaults to `false`.
//    public func authenticate(authTestOnly: Bool = false) async {
//        let kerbUtil = KerbUtil()
//        let kerbError = kerbUtil.getKerbCredentials(userPass, userPrincipal)
//        
//        do {
//            try await kerbUtil.getKerberosCredentialsAsync(userPass, userPrincipal)
//
//            while !kerbUtil.finished {
//                RunLoop.current.run(mode: RunLoop.Mode.default, before: Date.distantFuture)
//            }
//            logger.debug("kerbUtil returned: \(kerbError ?? "nil", privacy: .public)")
//            logger.debug("username: \(self.userPrincipal, privacy: .public)")
//            logger.debug("pass: \(self.userPass, privacy: .public)")
//
//            if let kerbError = kerbError {
//                // error
//                state = .kerbError
//                //TODO: Change to actual throws and error handling
//                switch kerbError {
//                case "Password has expired" :
//                    await delegate?.dogeADAuthenticationFailed(error: dogeADSessionError.PasswordExpired, description: kerbError)
//                    break
//                case "Wrong realm" :
//                    await delegate?.dogeADAuthenticationFailed(error: dogeADSessionError.wrongRealm, description: kerbError)
//                    break
//                case _ where kerbError.range(of: "unable to reach any KDC in realm") != nil :
//                    await delegate?.dogeADAuthenticationFailed(error: dogeADSessionError.OffDomain, description: kerbError)
//                    break
//                default:
//                    await delegate?.dogeADAuthenticationFailed(error: dogeADSessionError.KerbError, description: kerbError)
//                }
//            } else {
//                if authTestOnly {
//                    await klistUtil.kdestroy(princ: userPrincipal)
//                }
//                await delegate?.dogeADAuthenticationSucceded()
//            }
//        }
//    }
    public func authenticate(authTestOnly: Bool = false) async {
        let kerbUtil = KerbUtil()
        
        do {
            // Attempt to get Kerberos credentials
            try await kerbUtil.getKerberosCredentialsAsync(userPass, userPrincipal)
            
            // Destroy tickets if only testing authentication
            if authTestOnly {
                await klistUtil.kdestroy(princ: userPrincipal)
            }
            
            // Notify delegate of successful authentication
            await delegate?.dogeADAuthenticationSucceded()
            
        } catch let error as dogeADSessionError {
            // Handle specific Kerberos session errors
            userPass = "" // Ensure password is scrubbed even on error
            state = .kerbError
            
            let errorMessage: String
            switch error {
            case .PasswordExpired:
                errorMessage = "Password has expired"
            case .wrongRealm:
                errorMessage = "Wrong realm"
            case .OffDomain:
                errorMessage = "Unable to reach any KDC in realm"
            default:
                errorMessage = "Kerberos authentication error"
            }
            
            await delegate?.dogeADAuthenticationFailed(error: error, description: errorMessage)
            
        } catch {
            // Handle generic errors
            userPass = "" // Ensure password is scrubbed even on error
            state = .kerbError
            
            let errorMessage = error.localizedDescription
            let sessionError: dogeADSessionError
            
            switch errorMessage {
            case "Password has expired":
                sessionError = .PasswordExpired
            case "Wrong realm":
                sessionError = .wrongRealm
            case let desc where desc.contains("unable to reach any KDC in realm"):
                sessionError = .OffDomain
            default:
                sessionError = .KerbError
            }
            
            await delegate?.dogeADAuthenticationFailed(error: sessionError, description: errorMessage)
        }
    }

    /// Changes the password for the current user session.
    ///
    /// - Parameters:
    ///   - oldPassword: The current password of the user.
    ///   - newPassword: The new password to set for the user.
    ///   - completion: A closure that is called with an optional error message. If the operation is successful, the error message is `nil`.
    public func changePassword(oldPassword: String, newPassword: String, completion: @escaping (String?) -> Void) {
        logger.debug("Attempting to change Kerberos password for user: \(self.userPrincipal, privacy: .public)")
        
        KerbUtil().changeKerberosPassword(oldPassword, newPassword, userPrincipal) { [self] errorValue in
            if let errorValue = errorValue {
                logger.error("Failed to change password: \(errorValue, privacy: .public)")
                completion(errorValue)
            } else {
                logger.info("Password changed successfully for user: \(self.userPrincipal, privacy: .public)")
                completion(nil)
            }
        }
    }

    /// Changes the password for the current user session using a delegate.
    public func changePassword() async {
        // Prüfe Kerberos-Einstellungen
        logger.debug("Checking kpassword server.")
        _ = checkKpasswdServer()

        // Bereite KerbUtil vor
        logger.debug("Initializing KerbUtil.")
        let kerbUtil = KerbUtil()
        logger.debug("Attempting to change password.")
        
        do {
            // Versuche, das Passwort zu ändern
            try await kerbUtil.changeKerbPassword(oldPass, newPass, userPrincipal)
            
            // Aktualisiere das Passwortfeld
            userPass = newPass
            
            // Authentifiziere den Benutzer mit dem neuen Passwort
            await authenticate(authTestOnly: false)
            
            // Bereinige Passwörter und Kerberos-Einstellungen
            oldPass = ""
            newPass = ""
            cleanKerbPrefs()
            
            // Erfolgs-Delegate aufrufen
            await delegate?.dogeADAuthenticationSucceded()
            
        } catch {
            logger.error("Failed to change password: \(error.localizedDescription, privacy: .public)")
            
            // Fehler-Delegate aufrufen
            await delegate?.dogeADAuthenticationFailed(error: .KerbError, description: error.localizedDescription)
        }
    }

    /// Retrieves user information and updates the state based on Kerberos authentication and LDAP site discovery.
    public func userInfo() async {
        // Set initial state to offDomain
        state = .offDomain

        // Check for a valid Kerberos ticket
        await klistUtil.klist()
        if await !klistUtil.returnDefaultPrincipal().contains(kerberosRealm) && !anonymous {
            // No ticket for the specified realm
            await delegate?.dogeADAuthenticationFailed(error: .UnAuthenticated, description: "No ticket for Kerberos realm \(kerberosRealm)")
            return
        }

        // Configure SSL settings if necessary
        if useSSL {
            URIPrefix = "ldaps://"
            port = 636
            maxSSF = "-O maxssf=0 "
        }

        var lookupSite = true

        // Check for existing server connectivity and site information
        if let server = siteManager.sites[domain] {
            // Use existing server information
            lookupSite = false
            hosts = server
        } else {
            getHosts(for: domain)
        }

        // Set state to success if not looking up site
        if !lookupSite {
            state = .success
        }

        // If no LDAP servers are available, report as offDomain
        guard !hosts.isEmpty else {
            let errorMessage: String
            switch ldaptype {
            case .AD:
                errorMessage = "No AD Domain Controllers can be reached."
            case .OD:
                errorMessage = "No Open Directory servers can be reached."
            }

            await delegate?.dogeADAuthenticationFailed(error: .OffDomain, description: errorMessage)
            return
        }

        // Perform LDAP ping to determine the correct site if necessary
        if ldaptype == .AD && lookupSite {
            await findSite()
            if state != .success {
                await delegate?.dogeADAuthenticationFailed(error: .SiteError, description: "Unable to determine the correct site.")
                return
            }
        }

        // Test available hosts
        if !hosts.isEmpty {
            await testHosts()
        }

        // Update site information if necessary
        if lookupSite && !hosts.isEmpty {
            siteManager.updateSites(for: domain, with: hosts)
        }

        // Retrieve user information
        await getUserInformation()

        // Notify delegate with user information
        if let userRecord = userRecord {
            await delegate?.dogeADUserInformation(user: userRecord)
        }
    }

}

extension dogeADSession {
    // MARK: - testHosts with completion functionality

    /// Tests the connectivity of all hosts concurrently.
    ///
    /// - Returns: A Boolean indicating whether the domain status is successfully asserted.
    public func testHostsC() async -> Bool {
        guard state == .success else {
            logger.info("Status not success but \(self.state, privacy: .public)")
            return false
        }
        
        await withTaskGroup(of: Void.self) { group in
            for i in 0..<hosts.count where hosts[i].status != "dead" {
                group.addTask { [self] in
                    logger.info("Trying host: \(self.hosts[i].host, privacy: .public)")
                    
                    // Socket test first - this could be falsely negative
                    let cliTaskString = "/usr/bin/nc -G 5 -z \(self.hosts[i].host) \(self.port)"
                    
                    do {
                        let result = try await cliTask(cliTaskString)
                        await self.handleSocketResult(result: result, index: i)
                    } catch {
                        logger.error("Error testing host \(self.hosts[i].host, privacy: .public): \(error.localizedDescription, privacy: .public)")
                        await self.markHostAsDead(i)
                    }
                }
            }
        }
        
        logger.info("Notifying that testHost tasks have finished")
        return assertDomainStatus(assertionHosts: hosts)
    }

    private func assertDomainStatus(assertionHosts: [dogeADLDAPServer]) -> Bool {
        guard !assertionHosts.isEmpty else {
            logger.info("No hosts")
            return false
        }

        if let lastHost = assertionHosts.last, lastHost.status == "dead" {
            logger.info("All DCs are dead! You should really fix this.")
            state = .offDomain
            return false
        } else {
            logger.info("On domain!")
            state = .success
            return true
        }
    }

    /// Handles the result of a socket test for a specific host.
    ///
    /// - Parameters:
    ///   - result: The result of the socket test.
    ///   - index: The index of the host in the hosts array.
    private func handleSocketResult(result: String, index: Int) async {
        if result.contains("succeeded!") {
            let attribute = (ldaptype == .OD) ? "namingContexts" : "defaultNamingContext"
            
            let baseCommand = "/usr/bin/ldapsearch -N -LLL "
            let authParam = anonymous ? "-x " : "-Q "
            let fullCommand = "\(baseCommand)\(authParam)\(maxSSF)-l 3 -s base -H \(URIPrefix)\(hosts[index].host):\(port) \(attribute)"
            
            do {
                let ldapResult = try await cliTask(fullCommand)
                await handleSocketResultInternalCliTasks(result: ldapResult, index: index, attribute: attribute)
            } catch {
                logger.error("LDAP command failed for host \(self.hosts[index].host, privacy: .public): \(error.localizedDescription, privacy: .public)")
                await markHostAsDead(index)
            }
        } else {
            logger.info("Server is dead by way of socket test: \(self.hosts[index].host, privacy: .public)")
            await markHostAsDead(index)
        }
    }

    /// Internal function to handle the result of LDAP CLI tasks.
    ///
    /// - Parameters:
    ///   - result: The result of the LDAP command.
    ///   - index: The index of the host in the hosts array.
    ///   - attribute: The LDAP attribute being queried.
    private func handleSocketResultInternalCliTasks(result: String, index: Int, attribute: String) async {
        if !result.isEmpty && !result.contains("GSSAPI Error") && !result.contains("Can't contact") {
            let ldifResult = cleanLDIF(result)
            if !ldifResult.isEmpty {
                defaultNamingContext = getAttributeForSingleRecordFromCleanedLDIF(attribute, ldif: ldifResult)
                hosts[index].status = "live"
                hosts[index].timeStamp = Date()
                logger.info("Host \(self.hosts[index].host, privacy: .public) is live with default naming context: \(self.defaultNamingContext, privacy: .public)")
            } else {
                await markHostAsDead(index)
            }
        } else {
            await markHostAsDead(index)
        }
    }

    /// Handles the result of internal LDAP CLI tasks.
    ///
    /// - Parameters:
    ///   - result: The result of the LDAP command.
    ///   - index: The index of the host in the hosts array.
    ///   - attribute: The LDAP attribute being queried.
    private func handleSocketResultInternalCliTasks(result: String, index: Int, attribute: String) {
        // TODO: Consider implementing swapPrincipals(false) if necessary

        guard !result.isEmpty, !result.contains("GSSAPI Error"), !result.contains("Can't contact") else {
            markHostAsDead(index)
            return
        }

        let ldifResult = cleanLDIF(result)
        guard !ldifResult.isEmpty else {
            markHostAsDead(index)
            return
        }

        defaultNamingContext = getAttributeForSingleRecordFromCleanedLDIF(attribute, ldif: ldifResult)
        updateHostStatus(index: index, status: "live")
        logger.info("Current LDAP Server is: \(self.hosts[index].host, privacy: .public)")
        logger.info("Current default naming context: \(self.defaultNamingContext, privacy: .public)")
        current = index
    }

    /// Marks a host as dead.
    ///
    /// - Parameter index: The index of the host to mark as dead.
    private func markHostAsDead(_ index: Int) {
        logger.info("Server is dead by way of LDAP test: \(self.hosts[index].host, privacy: .public)")
        updateHostStatus(index: index, status: "dead")
    }

    /// Updates the status and timestamp of a host.
    ///
    /// - Parameters:
    ///   - index: The index of the host to update.
    ///   - status: The new status to set for the host.
    private func updateHostStatus(index: Int, status: String) {
        hosts[index].status = status
        hosts[index].timeStamp = Date()
    }
}

// Async wrapper for getKerberosCredentials
extension KerbUtil {
    /// Asynchronously retrieves Kerberos credentials for the specified user.
    ///
    /// - Parameters:
    ///   - userPass: The password of the user.
    ///   - userPrincipal: The principal of the user.
    /// - Throws: An `NSError` if the credentials cannot be retrieved.
    /// - Returns: Void
    func getKerberosCredentialsAsync(_ userPass: String, _ userPrincipal: String) async throws {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
            var observer: NSObjectProtocol?
            observer = NotificationCenter.default.addObserver(forName: .kerberosCredentialsDidFinish, object: nil, queue: .main) { notification in
                if let observer = observer {
                    NotificationCenter.default.removeObserver(observer)
                }
                
                if let error = notification.userInfo?["error"] as? String {
                    let nsError = NSError(domain: "KerbUtilError", code: 1, userInfo: [
                        NSLocalizedDescriptionKey: error
                    ])
                    continuation.resume(throwing: nsError)
                } else {
                    continuation.resume(returning: ())
                }
            }
            
            self.getKerberosCredentials(userPass, userPrincipal) { error in
                NotificationCenter.default.post(name: .kerberosCredentialsDidFinish, object: nil, userInfo: ["error": error as Any])
            }
        }
    }
}

extension Notification.Name {
    static let kerberosCredentialsDidFinish = Notification.Name("kerberosCredentialsDidFinish")
}
