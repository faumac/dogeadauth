//
//  CertificateSigningRequest.swift
//  OpportunisticRouting
//
//  Created by Corey Baker on 10/19/16.
//  Copyright © Corey Baker. All rights reserved.
//  Adapted and improved by Gregor Longariva on Oct/2024
//
//  This is a port of ios-csr by Ales Teska (https://github.com/ateska/ios-csr)
//  from Objective-c to Swift 3.0. Additions have been made to allow SHA256 and SHA512.
//
//
//  MIT License
//
//  Copyright (c) 2016 Corey Baker
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import dogeADAuthObjC
import CommonCrypto
import OSLog

// MARK: - ASN.1 Constants
private enum ASN1 {
    enum ObjectIdentifier {
        static let commonName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x03]
        static let countryName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x06]
        static let organizationName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x0A]
        static let organizationalUnitName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x0B]
        static let rsaEncryptionNULL: [UInt8] = [0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00]
    }
    
    enum AlgorithmIdentifier {
        static let sha1WithRSA: [UInt8] = [0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 1, 1, 5, 0x05, 0x00]
        static let sha256WithRSA: [UInt8] = [0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 1, 1, 0x0B, 0x05, 0x00]
        static let sha512WithRSA: [UInt8] = [0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 1, 1, 0x0D, 0x05, 0x00]
    }
    
    enum Tag {
        static let sequence: UInt8 = 0x30
        static let set: UInt8 = 0x31
        static let utf8String: UInt8 = 0x0C
        static let bitString: UInt8 = 0x03
        static let integer: UInt8 = 0x02
    }
}

/// Supported cryptographic algorithms for certificate signing requests
public enum CryptoAlgorithm {
    case md5, sha1, sha224, sha256, sha384, sha512
    
    var HMACAlgorithm: CCHmacAlgorithm {
        switch self {
        case .md5:      return CCHmacAlgorithm(kCCHmacAlgMD5)
        case .sha1:     return CCHmacAlgorithm(kCCHmacAlgSHA1)
        case .sha224:   return CCHmacAlgorithm(kCCHmacAlgSHA224)
        case .sha256:   return CCHmacAlgorithm(kCCHmacAlgSHA256)
        case .sha384:   return CCHmacAlgorithm(kCCHmacAlgSHA384)
        case .sha512:   return CCHmacAlgorithm(kCCHmacAlgSHA512)
        }
    }
    
    var digestLength: Int {
        switch self {
        case .md5:      return Int(CC_MD5_DIGEST_LENGTH)
        case .sha1:     return Int(CC_SHA1_DIGEST_LENGTH)
        case .sha224:   return Int(CC_SHA224_DIGEST_LENGTH)
        case .sha256:   return Int(CC_SHA256_DIGEST_LENGTH)
        case .sha384:   return Int(CC_SHA384_DIGEST_LENGTH)
        case .sha512:   return Int(CC_SHA512_DIGEST_LENGTH)
        }
    }
    
    var algorithmIdentifier: [UInt8] {
        switch self {
        case .sha1:     return ASN1.AlgorithmIdentifier.sha1WithRSA
        case .sha256:   return ASN1.AlgorithmIdentifier.sha256WithRSA
        case .sha512:   return ASN1.AlgorithmIdentifier.sha512WithRSA
        default:        return ASN1.AlgorithmIdentifier.sha1WithRSA
        }
    }
}

/// Class for creating and managing Certificate Signing Requests (CSR)
public final class CertificateSigningRequest: NSObject {
    
    // MARK: - Types
    
    /// Errors that can occur during CSR creation
    enum CSRError: Error {
        case invalidKeyFormat
        case signatureError
        case algorithmNotSupported
        case encodingError
    }
    
    // MARK: - Properties
    
    private let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "CertificateSigningRequest")
    
    private let countryName: String?
    private let organizationName: String?
    private let organizationUnitName: String?
    private let commonName: String?
    private var subjectDER: Data?
    private var cryptoAlgorithm: CryptoAlgorithm
    
    // MARK: - ASN.1 Constants
    
    private enum ASN1ObjectIdentifier {
        static let commonName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x03]
        static let countryName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x06]
        static let organizationName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x0A]
        static let organizationalUnitName: [UInt8] = [0x06, 0x03, 0x55, 0x04, 0x0B]
        static let rsaEncryptionNULL: [UInt8] = [0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00]
    }
    
    private enum ASN1AlgorithmIdentifier {
        static let sha1WithRSA: [UInt8] = [0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 1, 1, 5, 0x05, 0x00]
        static let sha256WithRSA: [UInt8] = [0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 1, 1, 0x0B, 0x05, 0x00]
        static let sha512WithRSA: [UInt8] = [0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 1, 1, 0x0D, 0x05, 0x00]
    }
    
    private enum ASN1Tag {
        static let sequence: UInt8 = 0x30
        static let set: UInt8 = 0x31
        static let utf8String: UInt8 = 0x0C
        static let bitString: UInt8 = 0x03
        static let integer: UInt8 = 0x02
    }
    
    // MARK: - Initialization
    
    /// Initialize a CSR with all available parameters
    /// - Parameters:
    ///   - commonName: The common name for the certificate
    ///   - organizationName: The organization name
    ///   - organizationUnitName: The organization unit name
    ///   - countryName: The country name
    ///   - cryptoAlgorithm: The cryptographic algorithm to use
    public init(commonName: String? = nil,
                organizationName: String? = nil,
                organizationUnitName: String? = nil,
                countryName: String? = nil,
                cryptoAlgorithm: CryptoAlgorithm = .sha1) {
        self.commonName = commonName
        self.organizationName = organizationName
        self.organizationUnitName = organizationUnitName
        self.countryName = countryName
        self.cryptoAlgorithm = cryptoAlgorithm
        super.init()
    }
    
    // MARK: - Public Methods
    
    /// Builds the CSR using the provided public key and private key
    /// - Parameters:
    ///   - publicKeyBits: The public key data
    ///   - privateKey: The private key for signing
    /// - Returns: The generated CSR data or nil if the process fails
    public func build(_ publicKeyBits: Data, privateKey: SecKey) -> Data? {
        do {
            let requestInfo = try buildCertificationRequestInfo(publicKeyBits)
            let signature = try createSignature(for: requestInfo, with: privateKey)
            return try buildFinalCSR(with: requestInfo, signature: signature)
        } catch {
            logger.error("CSR creation failed: \(error.localizedDescription)")
            return nil
        }
    }
    
    // MARK: - Private Methods
    
    private func buildCertificationRequestInfo(_ publicKeyBits: Data) throws -> Data {
        var requestInfo = Data()
        
        // Version
        let version: [UInt8] = [ASN1Tag.integer, 0x01, 0x00]
        requestInfo.append(version, count: version.count)
        
        // Subject
        let subject = try buildSubject()
        requestInfo.append(subject)
        
        // Public Key Info
        let publicKeyInfo = try buildPublicKeyInfo(publicKeyBits)
        requestInfo.append(publicKeyInfo)
        
        // Attributes
        let attributes: [UInt8] = [0xA0, 0x00]
        requestInfo.append(attributes, count: attributes.count)
        
        // Wrap in SEQUENCE
        enclose(&requestInfo, by: ASN1Tag.sequence)
        
        return requestInfo
    }
    
    private func buildSubject() throws -> Data {
        var subject = Data()
        
        if let countryName = countryName {
            try appendSubjectItem(ASN1ObjectIdentifier.countryName, value: countryName, into: &subject)
        }
        
        if let organizationName = organizationName {
            try appendSubjectItem(ASN1ObjectIdentifier.organizationName, value: organizationName, into: &subject)
        }
        
        if let organizationUnitName = organizationUnitName {
            try appendSubjectItem(ASN1ObjectIdentifier.organizationalUnitName, value: organizationUnitName, into: &subject)
        }
        
        if let commonName = commonName {
            try appendSubjectItem(ASN1ObjectIdentifier.commonName, value: commonName, into: &subject)
        }
        
        enclose(&subject, by: ASN1Tag.sequence)
        subjectDER = subject
        
        return subject
    }
    
    private func buildPublicKeyInfo(_ publicKeyBits: Data) throws -> Data {
        var publicKeyInfo = Data()
        
        // Algorithm Identifier
        publicKeyInfo.append(ASN1ObjectIdentifier.rsaEncryptionNULL, count: ASN1ObjectIdentifier.rsaEncryptionNULL.count)
        enclose(&publicKeyInfo, by: ASN1Tag.sequence)
        
        // Public Key
        var publicKeyASN = Data()
        
        let modulus = try getPublicKeyModulus(publicKeyBits)
        publicKeyASN.append(ASN1Tag.integer)
        appendDERLength(modulus.count, into: &publicKeyASN)
        publicKeyASN.append(modulus)
        
        let exponent = try getPublicKeyExponent(publicKeyBits)
        publicKeyASN.append(ASN1Tag.integer)
        appendDERLength(exponent.count, into: &publicKeyASN)
        publicKeyASN.append(exponent)
        
        enclose(&publicKeyASN, by: ASN1Tag.sequence)
        prependByte(0x00, into: &publicKeyASN)
        
        appendBITSTRING(publicKeyASN, into: &publicKeyInfo)
        enclose(&publicKeyInfo, by: ASN1Tag.sequence)
        
        return publicKeyInfo
    }
    
    
    // MARK: - Private Methods - DER Encoding
    
    /// Appends a subject item to the given data buffer
    /// - Parameters:
    ///   - identifier: The ASN.1 object identifier
    ///   - value: The value to append
    ///   - buffer: The buffer to append to
    private func appendSubjectItem(_ identifier: [UInt8], value: String, into buffer: inout Data) throws {
        guard identifier.count == 5 else {
            logger.error("Invalid subject identifier length")
            throw CSRError.encodingError
        }
        
        var subjectItem = Data()
        subjectItem.append(identifier, count: identifier.count)
        try appendUTF8String(value, into: &subjectItem)
        enclose(&subjectItem, by: ASN1.Tag.sequence)
        enclose(&subjectItem, by: ASN1.Tag.set)
        
        buffer.append(subjectItem)
    }
    
    /// Appends a UTF8 string to the given data buffer
    /// - Parameters:
    ///   - string: The string to append
    ///   - buffer: The buffer to append to
    private func appendUTF8String(_ string: String, into buffer: inout Data) throws {
        guard let stringData = string.data(using: .utf8) else {
            logger.error("Failed to encode string as UTF-8")
            throw CSRError.encodingError
        }
        
        buffer.append(ASN1.Tag.utf8String)
        appendDERLength(stringData.count, into: &buffer)
        buffer.append(stringData)
    }
    
    /// Appends DER length bytes to the given data buffer
    /// - Parameters:
    ///   - length: The length to encode
    ///   - buffer: The buffer to append to
    private func appendDERLength(_ length: Int, into buffer: inout Data) {
        precondition(length < 0x8000, "Length too large for DER encoding")
        
        if length < 128 {
            buffer.append(UInt8(length))
        } else if length < 0x100 {
            buffer.append(0x81)
            buffer.append(UInt8(length & 0xFF))
        } else {
            buffer.append(0x82)
            buffer.append(UInt8((length >> 8) & 0xFF))
            buffer.append(UInt8(length & 0xFF))
        }
    }
    
    /// Appends a BIT STRING to the given data buffer
    /// - Parameters:
    ///   - data: The data to append
    ///   - buffer: The buffer to append to
    private func appendBITSTRING(_ data: Data, into buffer: inout Data) {
        buffer.append(ASN1Tag.bitString)
        appendDERLength(data.count, into: &buffer)
        buffer.append(data)
    }
    
    /// Encloses the given data with a tag
    /// - Parameters:
    ///   - data: The data to enclose
    ///   - tag: The tag to use
    private func enclose(_ data: inout Data, by tag: UInt8) {
        var newData = Data(capacity: data.count + 4)
        newData.append(tag)
        appendDERLength(data.count, into: &newData)
        newData.append(data)
        data = newData
    }
    
    /// Prepends a byte to the given data
    /// - Parameters:
    ///   - byte: The byte to prepend
    ///   - data: The data to prepend to
    private func prependByte(_ byte: UInt8, into data: inout Data) {
        var newData = Data(capacity: data.count + 1)
        newData.append(byte)
        newData.append(data)
        data = newData
    }
    
    // MARK: - Private Methods - Public Key Processing
    
    /// Extracts the public key exponent from the given public key data
    /// - Parameter publicKeyBits: The public key data
    /// - Returns: The exponent data
    private func getPublicKeyExponent(_ publicKeyBits: Data) throws -> Data {
        var iterator = 0
        
        // Skip TYPE - bit stream - mod + exp
        iterator += 1
        _ = try derEncodingGetSize(from: publicKeyBits, at: &iterator)
        
        // Skip TYPE - bit stream mod
        iterator += 1
        let modSize = try derEncodingGetSize(from: publicKeyBits, at: &iterator)
        iterator += modSize
        
        // Get exponent
        iterator += 1
        let expSize = try derEncodingGetSize(from: publicKeyBits, at: &iterator)
        
        guard publicKeyBits.count >= iterator + expSize else {
            throw CSRError.invalidKeyFormat
        }
        
        return publicKeyBits.subdata(in: iterator..<(iterator + expSize))
    }
    
    /// Extracts the public key modulus from the given public key data
    /// - Parameter publicKeyBits: The public key data
    /// - Returns: The modulus data
    private func getPublicKeyModulus(_ publicKeyBits: Data) throws -> Data {
        var iterator = 0
        
        // Skip TYPE - bit stream - mod + exp
        iterator += 1
        _ = try derEncodingGetSize(from: publicKeyBits, at: &iterator)
        
        // Get modulus
        iterator += 1
        let modSize = try derEncodingGetSize(from: publicKeyBits, at: &iterator)
        
        guard publicKeyBits.count >= iterator + modSize else {
            throw CSRError.invalidKeyFormat
        }
        
        return publicKeyBits.subdata(in: iterator..<(iterator + modSize))
    }
    
    /// Gets the size from DER encoding at the given position
    /// - Parameters:
    ///   - buffer: The buffer to read from
    ///   - iterator: The position to read at
    /// - Returns: The size value
    private func derEncodingGetSize(from buffer: Data, at iterator: inout Int) throws -> Int {
        guard buffer.count > iterator else {
            throw CSRError.encodingError
        }
        
        let data = [UInt8](buffer)
        var numOfBytes = 1
        var result = 0
        
        if data[iterator] > 0x80 {
            numOfBytes = Int(data[iterator] - 0x80)
            iterator += 1
        }
        
        guard buffer.count >= iterator + numOfBytes else {
            throw CSRError.encodingError
        }
        
        for index in 0..<numOfBytes {
            result = (result * 0x100) + Int(data[iterator + index])
        }
        
        iterator += numOfBytes
        return result
    }
    
    // MARK: - Private Methods - Signature Creation
    
    /// Creates a signature for the given request info
    /// - Parameters:
    ///   - requestInfo: The request info to sign
    ///   - privateKey: The private key to use for signing
    /// - Returns: The signature data
    private func createSignature(for requestInfo: Data, with privateKey: SecKey) throws -> Data {
        let digest = try createDigest(for: requestInfo)
        return try signDigest(digest, with: privateKey)
    }
    
    /// Creates a digest for the given data using the selected algorithm
    /// - Parameter data: The data to digest
    /// - Returns: The digest data
    private func createDigest(for data: Data) throws -> Data {
        var digest = [UInt8](repeating: 0, count: cryptoAlgorithm.digestLength)
        var buffer = [UInt8](repeating: 0, count: data.count)
        data.copyBytes(to: &buffer, count: data.count)
        
        switch cryptoAlgorithm {
        case .sha1:
            var context = CC_SHA1_CTX()
            CC_SHA1_Init(&context)
            CC_SHA1_Update(&context, buffer, CC_LONG(data.count))
            CC_SHA1_Final(&digest, &context)
            
        case .sha256:
            var context = CC_SHA256_CTX()
            CC_SHA256_Init(&context)
            CC_SHA256_Update(&context, buffer, CC_LONG(data.count))
            CC_SHA256_Final(&digest, &context)
            
        case .sha512:
            var context = CC_SHA512_CTX()
            CC_SHA512_Init(&context)
            CC_SHA512_Update(&context, buffer, CC_LONG(data.count))
            CC_SHA512_Final(&digest, &context)
            
        default:
            throw CSRError.algorithmNotSupported
        }
        
        return Data(digest)
    }
    
    /// Signs the given digest with the private key
    /// - Parameters:
    ///   - digest: The digest to sign
    ///   - privateKey: The private key to use
    /// - Returns: The signature data
    private func signDigest(_ digest: Data, with privateKey: SecKey) throws -> Data {
        var error: Unmanaged<CFError>?
        
        guard let signer = SecSignTransformCreate(privateKey, &error) else {
            throw error?.takeRetainedValue() ?? CSRError.signatureError
        }
        
        SecTransformSetAttribute(signer, kSecTransformInputAttributeName, digest as CFData, &error)
        SecTransformSetAttribute(signer, kSecInputIsAttributeName, kSecInputIsRaw, &error)
        SecTransformSetAttribute(signer, kSecPaddingKey, kSecPaddingPKCS1Key, &error)
        
        guard let signature = SecTransformExecute(signer, &error) as? Data else {
            throw error?.takeRetainedValue() ?? CSRError.signatureError
        }
        
        return signature
    }
    
    /// Builds the final CSR by combining the request info and signature
    /// - Parameters:
    ///   - requestInfo: The request info
    ///   - signature: The signature
    /// - Returns: The complete CSR data
    private func buildFinalCSR(with requestInfo: Data, signature: Data) throws -> Data {
        var csr = Data()
        
        // Add request info
        csr.append(requestInfo)
        
        // Add algorithm identifier
        csr.append(cryptoAlgorithm.algorithmIdentifier, count: cryptoAlgorithm.algorithmIdentifier.count)
        
        // Add signature
        var signatureData = Data()
        signatureData.append(0x00) // Prepend zero
        signatureData.append(signature)
        appendBITSTRING(signatureData, into: &csr)
        
        // Wrap in SEQUENCE
        enclose(&csr, by: ASN1Tag.sequence)
        
        return csr
    }
}
