//
//  DNSResolver.m
//  dogeADAuth
//
//  Created by Boushy, Phillip on 9/28/16.
//  Copyright © 2016 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 18.12.24.
//  Copyright © 2024 Regionales Rechenzentrum Erlangen. All rights reserved.
//

#import "DNSResolver.h"

#include <dns_util.h>
#include <net/if.h>

@interface DNSResolver ()

/// Indicates whether the DNS query has finished.
@property (nonatomic, assign, readwrite) BOOL finished;

/// Stores any error that occurred during the DNS query.
@property (nonatomic, copy, readwrite) NSError *error;

/// An array to hold the mutable query responses.
@property (nonatomic, strong, readonly) NSMutableArray *mutableQueryResponse;

@end

@implementation DNSResolver {
    DNSServiceRef _dnsService;
    CFSocketRef _dnsSocket;
}

@synthesize queryType = _queryType;
@synthesize queryValue = _queryValue;
@synthesize delegate = _delegate;

/// Initializes a new instance of DNSResolver.
-  (instancetype)init {
    self = [super init];
    if (self) {
        _mutableQueryResponse = [[NSMutableArray alloc] init];
    }
    return self;
}

/// Initializes a new instance of DNSResolver with a specified query type and value.
/// @param queryType The type of DNS query to perform (e.g., "SRV", "PTR").
/// @param queryValue The value to query for.
-  (instancetype)initWithQueryType:(NSString *)queryType andValue:(NSString *)queryValue {
    NSParameterAssert(queryType != nil);
    NSParameterAssert(queryValue != nil);
    self = [super init];
    if (self) {
        _queryType = [queryType copy];
        _queryValue = [queryValue copy];
        _mutableQueryResponse = [[NSMutableArray alloc] init];
        NSAssert(_mutableQueryResponse != nil, @"Failed to initialize mutableQueryResponse");
    }
    return self;
}

/// Starts the DNS query.
-  (void)startQuery {
    if (_dnsService == NULL) {
        self.error = nil;
        self.finished = NO;
        [_mutableQueryResponse removeAllObjects];
        [self startInternal];
    }
}

/// Converts the query type to its corresponding integer value.
/// @return The integer value representing the DNS record type.
-  (uint16_t)getTypeAsInt {
    if ([self.queryType isEqualToString:@"SRV"]) {
        return kDNSServiceType_SRV;
    } else if ([self.queryType isEqualToString:@"PTR"]) {
        return kDNSServiceType_PTR;
    } else {
        return kDNSServiceType_ANY;
    }
}

/// Internal method to start the DNS query process.
-  (void)startInternal {
    DNSServiceErrorType err = kDNSServiceErr_NoError;
    const char *dnsNameCStr = [self.queryValue UTF8String];
    int socketProtocol;
    int flags;

    // Ensure dnsNameCStr is not NULL
    if (dnsNameCStr == NULL) {
        [self stopQueryWithDNSServiceError:kDNSServiceErr_BadParam];
        return;
    }

    uint16_t recordType = [self getTypeAsInt];

    if ([self.queryValue hasSuffix:@".local"]) {
        flags = (kDNSServiceFlagsReturnIntermediates + kDNSServiceFlagsTimeout);
    } else {
        flags = kDNSServiceFlagsReturnIntermediates;
    }

    // Create the DNS Query
    err = DNSServiceQueryRecord(
        &_dnsService,
        flags,
        0, // query on all interfaces.
        dnsNameCStr,
        recordType,
        kDNSServiceClass_IN,
        DNSServiceRecordCallback,
        (__bridge void *)self
    );

    if (err == kDNSServiceErr_NoError) {
        socketProtocol = DNSServiceRefSockFD(_dnsService);
        CFSocketContext context = {0, (__bridge void *)self, NULL, NULL, NULL};
        _dnsSocket = CFSocketCreateWithNative(
            NULL,
            socketProtocol,
            kCFSocketReadCallBack,
            DNSSocketCallback,
            &context
        );

        if (_dnsSocket) {
            CFRunLoopSourceRef runLoopSource = CFSocketCreateRunLoopSource(NULL, _dnsSocket, 0);
            if (runLoopSource) {
                CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopDefaultMode);
                CFRelease(runLoopSource);
            }
        }
    }

    if (err != kDNSServiceErr_NoError) {
        [self stopQueryWithDNSServiceError:err];
    }
}

/// Callback function for DNS service records.
static void DNSServiceRecordCallback(
    DNSServiceRef dnsService,
    DNSServiceFlags flags,
    uint32_t interfaceIndex,
    DNSServiceErrorType errorCode,
    const char *fullname,
    uint16_t recordType,
    uint16_t recordClass,
    uint16_t recordLength,
    const void *recordData,
    uint32_t ttl,
    void *context
) {
    DNSResolver *obj = (__bridge DNSResolver *)context;

    if (errorCode == kDNSServiceErr_NoError) {
        [obj processRecord:recordData length:recordLength];

        if (!(flags & kDNSServiceFlagsMoreComing)) {
            [obj stopQueryWithError:nil];
        }
    } else {
        [obj stopQueryWithDNSServiceError:errorCode];
    }
}

/// Callback function for DNS socket events.
static void DNSSocketCallback(
    CFSocketRef dnsSocket,
    CFSocketCallBackType type,
    CFDataRef address,
    const void *data,
    void *info
) {
    DNSResolver *obj = (__bridge DNSResolver *)info;
    DNSServiceErrorType err = DNSServiceProcessResult(obj->_dnsService);
    if (err != kDNSServiceErr_NoError) {
        [obj stopQueryWithDNSServiceError:err];
    }
}

/// Processes a DNS record.
/// @param recordData The raw data of the DNS record.
/// @param recordLength The length of the DNS record data.
-  (void)processRecord:(const void *)recordData length:(NSUInteger)recordLength {
    NSMutableData *resourceRecordData = [NSMutableData data];
    dns_resource_record_t *resourceRecord;
    uint8_t u8 = 0;
    uint16_t u16;
    uint32_t u32;

    [resourceRecordData appendBytes:&u8 length:sizeof(u8)];
    uint16_t recordType = [self getTypeAsInt];
    u16 = htons(recordType);
    [resourceRecordData appendBytes:&u16 length:sizeof(u16)];
    u16 = htons(kDNSServiceClass_IN);
    [resourceRecordData appendBytes:&u16 length:sizeof(u16)];
    u32 = htonl(666);
    [resourceRecordData appendBytes:&u32 length:sizeof(u32)];
    u16 = htons(recordLength);
    [resourceRecordData appendBytes:&u16 length:sizeof(u16)];
    [resourceRecordData appendBytes:recordData length:recordLength];

    resourceRecord = dns_parse_resource_record([resourceRecordData bytes], (uint32_t)[resourceRecordData length]);

    if (resourceRecord != NULL) {
        if ([self.queryType isEqualToString:@"SRV"]) {
            NSString *target = [NSString stringWithCString:resourceRecord->data.SRV->target encoding:NSASCIIStringEncoding];
            if (target != nil) {
                NSDictionary *result = @{
                    kSRVResolverPriority : @(resourceRecord->data.SRV->priority),
                    kSRVResolverWeight: @(resourceRecord->data.SRV->weight),
                    kSRVResolverPort: @(resourceRecord->data.SRV->port),
                    kSRVResolverTarget: target
                };
                
                NSIndexSet *resultIndexSet = [NSIndexSet indexSetWithIndex:self.queryResults.count];
                
                [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:resultIndexSet forKey:@"results"];
                [self.mutableQueryResponse addObject:result];
                [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:resultIndexSet forKey:@"results"];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(dnsResolver:didReceiveQueryResult:)]) {
                    [self.delegate dnsResolver:self didReceiveQueryResult:result];
                }
            }
        }
        
        dns_free_resource_record(resourceRecord);
    }
}

# pragma mark - Stop Query Methods

/// Stops the DNS query.
-  (void)stopQuery {
    if (_dnsSocket != NULL) {
        CFSocketInvalidate(_dnsSocket);
        CFRelease(_dnsSocket);
        _dnsSocket = NULL;
    }
    if (_dnsService != NULL) {
        DNSServiceRefDeallocate(_dnsService);
        _dnsService = NULL;
    }
    self.finished = YES;
}

/// Stops the DNS query with an error.
/// @param error The error that caused the query to stop.
-  (void)stopQueryWithError:(NSError *)error {
    self.error = error;
    [self stopQuery];
    if (self.delegate && [self.delegate respondsToSelector:@selector(dnsResolver:didStopQueryWithError:)]) {
        [self.delegate dnsResolver:self didStopQueryWithError:error];
    }
}

/// Stops the DNS query with a DNS service error.
/// @param errorCode The DNS service error code.
-  (void)stopQueryWithDNSServiceError:(DNSServiceErrorType)errorCode {
    NSError *error = nil;
    if (errorCode != kDNSServiceErr_NoError) {
        error = [NSError errorWithDomain:kDNSResolverErrorDomain code:errorCode userInfo:nil];
    }
    [self stopQueryWithError:error];
}

# pragma mark - Results

/// Returns the results of the DNS query.
/// @return An array of query results.
-  (NSArray *)queryResults {
    return [self.mutableQueryResponse copy];
}

@end

NSString *kSRVResolverPriority = @"priority";
NSString *kSRVResolverWeight = @"weight";
NSString *kSRVResolverPort = @"port";
NSString *kSRVResolverTarget = @"target";

NSString *kDNSResolverErrorDomain = @"kDNSResolverErrorDomain";
