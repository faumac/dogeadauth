//
//  KerbUtil.m
//  dogeADAuth
//
//  Created by Joel Rennich on 4/26/16.
//  Copyright © 2016 Orchard & Grove Inc. All rights reserved.
//  Adapted by Longariva, Gregor (RRZE) on 18.12.24.
//  Copyright © 2024 Regionales Rechenzentrum Erlangen. All rights reserved.
//

#import "KerbUtil.h"
#import <GSS/GSS.h>
#import <krb5/krb5.h>
#import <Cocoa/Cocoa.h>
#import <Security/Security.h>
#import <DirectoryService/DirectoryService.h>
#import <OpenDirectory/OpenDirectory.h>
#import <os/log.h>

@interface KerbUtil ()

@property (nonatomic, assign, readwrite) BOOL finished;

@end

@implementation KerbUtil

// Declare private functions
extern OSStatus SecKeychainChangePassword(SecKeychainRef keychainRef, UInt32 oldPasswordLength, const void* oldPassword, UInt32 newPasswordLength, const void* newPassword);
extern OSStatus SecKeychainResetLogin(UInt32 passwordLength, const void* password, Boolean resetSearchList);

/**
 Retrieves Kerberos credentials for the specified user asynchronously.

 @param password The password of the user.
 @param userPrincipal The principal of the user.
 @param callback A completion block that returns an error message if the operation fails, or `nil` if successful.
 */
-  (void)getKerberosCredentials:(NSString *)password :(NSString *)userPrincipal completion:(void(^)(NSString *))callback {
    OM_uint32 maj_stat;
    gss_name_t gname = GSS_C_NO_NAME;
    gss_cred_id_t cred = NULL;
    CFErrorRef error = NULL;

    // Preflight for spaces in the userPrincipal
    gname = GSSCreateName((__bridge CFTypeRef)userPrincipal, GSS_C_NT_USER_NAME, NULL);
    if (gname == NULL) {
        callback(@"Error: Failed to create GSS name");
        return;
    }

    NSDictionary *attrs = @{ (id)kGSSICPassword : password };
    maj_stat = gss_aapl_initial_cred(gname, GSS_KRB5_MECHANISM, (__bridge CFDictionaryRef)attrs, &cred, &error);

    CFRelease(gname);
    if (maj_stat) {
        os_log_error(OS_LOG_DEFAULT, "Error: %d %@", (int)maj_stat, error);
        NSDictionary *errorDict = CFBridgingRelease(CFErrorCopyUserInfo(error));
        NSString *errorMessage = [errorDict valueForKey:(@"NSDescription")];
        callback(errorMessage);
    } else {
        callback(nil);
    }
    if (cred) CFRelease(cred);
}

/**
 Retrieves Kerberos credentials for the specified user synchronously.

 @param password The password of the user.
 @param userPrincipal The principal of the user.
 @return An error message if the operation fails, or `nil` if successful.
 */
-  (NSString *)getKerbCredentials:(NSString *)password :(NSString *)userPrincipal {
    self.finished = NO;
    OM_uint32 maj_stat;
    gss_name_t gname = GSS_C_NO_NAME;
    gss_cred_id_t cred = NULL;
    CFErrorRef error = NULL;

    // Preflight for spaces in the userPrincipal
    gname = GSSCreateName((__bridge CFTypeRef)userPrincipal, GSS_C_NT_USER_NAME, NULL);
    if (gname == NULL) {
        self.finished = YES;
        return @"Error: Creating GSS name";
    }

    NSDictionary *attrs = @{ (id)kGSSICPassword : password };
    maj_stat = gss_aapl_initial_cred(gname, GSS_KRB5_MECHANISM, (__bridge CFDictionaryRef)attrs, &cred, &error);

    CFRelease(gname);
    if (maj_stat) {
        os_log_error(OS_LOG_DEFAULT, "Error: %d %@", (int)maj_stat, error);
        NSDictionary *errorDict = CFBridgingRelease(CFErrorCopyUserInfo(error));
        self.finished = YES;
        return [errorDict valueForKey:(@"NSDescription")];
    }

    if (cred) CFRelease(cred);
    self.finished = YES;
    return nil;
}

/**
 Changes the Kerberos password for the specified user asynchronously.

 @param oldPassword The current password of the user.
 @param newPassword The new password to set for the user.
 @param userPrincipal The principal of the user.
 @param callback A completion block that returns an error message if the operation fails, or `nil` if successful.
 */
-  (void)changeKerberosPassword:(NSString *)oldPassword :(NSString *)newPassword :(NSString *)userPrincipal completion:(void(^)(NSString *))callback {
    self.finished = NO;
    OM_uint32 maj_stat;
    gss_name_t gname = GSS_C_NO_NAME;
    CFErrorRef error = NULL;

    gname = GSSCreateName((__bridge CFTypeRef)userPrincipal, GSS_C_NT_USER_NAME, NULL);
    if (gname == NULL) {
        callback(@"Error: Failed to create GSS name");
        self.finished = YES;
        return;
    }

    NSDictionary *attrs = @{ (id)kGSSChangePasswordOldPassword: oldPassword,
                             (id)kGSSChangePasswordNewPassword: newPassword };

    maj_stat = gss_aapl_change_password(gname, GSS_KRB5_MECHANISM, (__bridge CFDictionaryRef)attrs, &error);

    CFRelease(gname);
    if (maj_stat) {
        os_log_error(OS_LOG_DEFAULT, "Error: %d %@", (int)maj_stat, error);
        NSDictionary *errorDict = CFBridgingRelease(CFErrorCopyUserInfo(error));
        NSString *errorMessage = [errorDict valueForKey:(@"NSDescription")];
        callback(errorMessage);
    } else {
        callback(nil);
    }
    self.finished = YES;
}

/**
 Changes the Kerberos password for the specified user synchronously.

 @param oldPassword The current password of the user.
 @param newPassword The new password to set for the user.
 @param userPrincipal The principal of the user.
 @return An error message if the operation fails, or `nil` if successful.
 */
-  (NSString *)changeKerbPassword:(NSString *)oldPassword :(NSString *)newPassword :(NSString *)userPrincipal {
    self.finished = NO;
    OM_uint32 maj_stat;
    gss_name_t gname = GSS_C_NO_NAME;
    CFErrorRef error = NULL;

    gname = GSSCreateName((__bridge CFTypeRef)userPrincipal, GSS_C_NT_USER_NAME, NULL);
    if (gname == NULL) {
        self.finished = YES;
        return @"Error: Creating the GSS name.";
    }

    NSDictionary *attrs2 = @{ (id)kGSSChangePasswordOldPassword : oldPassword,
                              (id)kGSSChangePasswordNewPassword : newPassword };

    maj_stat = gss_aapl_change_password(gname, GSS_KRB5_MECHANISM, (__bridge CFDictionaryRef)attrs2, &error);
    CFRelease(gname);
    if (maj_stat) {
        os_log_error(OS_LOG_DEFAULT, "Error: %d %@", (int)maj_stat, error);
        NSDictionary *errorDict = CFBridgingRelease(CFErrorCopyUserInfo(error));
        self.finished = YES;
        return [errorDict valueForKey:(@"NSDescription")];
    }

    self.finished = YES;
    return nil;
}

/**
 Checks the given password against the system login.

 @param myPassword The password to check.
 @return `1` if the password is correct, `0` otherwise.
 */
-  (int)checkPassword:(NSString *)myPassword {
    AuthorizationItem myAuthRight = {
        .name = "system.login.tty",
        .value = NULL,
        .valueLength = 0,
        .flags = 0
    };
    AuthorizationRights authRights = {
        .count = 1,
        .items = &myAuthRight
    };

    AuthorizationItem authEnvironmentItems[2] = {
        {
            .name = kAuthorizationEnvironmentUsername,
            .valueLength = NSUserName().length,
            .value = (void *)[NSUserName() UTF8String],
            .flags = 0
        },
        {
            .name = kAuthorizationEnvironmentPassword,
            .valueLength = myPassword.length,
            .value = (void *)[myPassword UTF8String],
            .flags = 0
        }
    };
    AuthorizationEnvironment authEnvironment = {
        .count = 2,
        .items = authEnvironmentItems
    };

    OSStatus authStatus = AuthorizationCreate(&authRights, &authEnvironment, kAuthorizationFlagExtendRights, NULL);
    return (authStatus == errAuthorizationSuccess);
}

/**
 Changes the password of the default keychain.

 @param oldPassword The current keychain password.
 @param newPassword The new keychain password.
 @return `1` if the password was changed successfully, `0` otherwise.
 */
-  (int)changeKeychainPassword:(NSString *)oldPassword :(NSString *)newPassword {
    SecKeychainRef myDefaultKeychain;
    OSErr err = SecKeychainCopyDomainDefault(kSecPreferencesDomainUser, &myDefaultKeychain);
    if (err != noErr) {
        os_log_error(OS_LOG_DEFAULT, "Failed to get the default keychain. Error: %d", err);
        return 0;
    }

    UInt32 oldLength = (UInt32)oldPassword.length;
    UInt32 newLength = (UInt32)newPassword.length;
    const char *cStyleOldPassword = [oldPassword UTF8String];
    const char *cStyleNewPassword = [newPassword UTF8String];

    os_log(OS_LOG_DEFAULT, "Changing keychain password");
    err = SecKeychainChangePassword(myDefaultKeychain, oldLength, cStyleOldPassword, newLength, cStyleNewPassword);

    if (err == noErr) {
        os_log(OS_LOG_DEFAULT, "Password changed successfully");
        CFRelease(myDefaultKeychain);
        return 1;
    } else if (err == -25293) {
        err = SecKeychainChangePassword(myDefaultKeychain, oldLength, cStyleOldPassword, newLength, cStyleNewPassword);
        if (err == noErr) {
            os_log(OS_LOG_DEFAULT, "Password changed successfully");
            CFRelease(myDefaultKeychain);
            return 1;
        } else {
            os_log_error(OS_LOG_DEFAULT, "Bad password. Keychain change was not successful.");
            CFRelease(myDefaultKeychain);
            return 0;
        }
    } else {
        os_log_error(OS_LOG_DEFAULT, "Keychain change error: %d", err);
        CFRelease(myDefaultKeychain);
        return 0;
    }
}

/**
 Resets the login keychain with the given password.

 @param password The password to set for the login keychain.
 @return An `OSStatus` indicating success or failure.
 */
-  (OSStatus)resetKeychain:(NSString *)password {
    return SecKeychainResetLogin((UInt32)password.length, [password UTF8String], YES);
}

@end
