//
//  dogeADAuth.h
//  dogeADAuth
//
//  Created by Gregor Longariva on 14.12.2023.
//  Copyright © 2023 RRZE/FAU. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DNSResolver.h"
#import "KerbUtil.h"
#import "GSSItem.h"
#import "krb5.h"
#import <CommonCrypto/CommonHMAC.h>

//! Project version number for dogeADAuth.
FOUNDATION_EXPORT double dogeADAuthVersionNumber;

//! Project version string for dogeADAuth.
FOUNDATION_EXPORT const unsigned char dogeADAuthVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <dogeADAuth/PublicHeader.h>


